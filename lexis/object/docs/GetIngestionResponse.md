# LexisDataLake.Model.GetIngestionResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MultipartUpload** | [**ObjectMultipartUploadProperties**](ObjectMultipartUploadProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Ingestion** | [**GetIngestionProperties**](GetIngestionProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

