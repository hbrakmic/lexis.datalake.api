# LexisDataLake.Model.DescribeChangesetsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextToken** | **string** |  | [optional] 
**ItemCount** | **int?** |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Changesets** | [**List&lt;DescribeChangesetsResponseChangesets&gt;**](DescribeChangesetsResponseChangesets.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

