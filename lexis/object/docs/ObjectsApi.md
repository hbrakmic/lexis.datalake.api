# LexisDataLake.Api.ObjectsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateObject**](ObjectsApi.md#createobject) | **POST** /{objectId} | Create an Object
[**DeleteObject**](ObjectsApi.md#deleteobject) | **DELETE** /{objectId} | Delete an Object
[**DeleteObjects**](ObjectsApi.md#deleteobjects) | **DELETE** / | Delete a batch of Objects
[**DescribeObjects**](ObjectsApi.md#describeobjects) | **GET** /describe | Describe Objects
[**GetObject**](ObjectsApi.md#getobject) | **GET** /{objectId} | Get an Object by ID
[**ListObjects**](ObjectsApi.md#listobjects) | **GET** / | List Objects
[**UpdateObject**](ObjectsApi.md#updateobject) | **PUT** /{objectId} | Update an Object

<a name="createobject"></a>
# **CreateObject**
> ObjectResponse CreateObject (ObjectRequest body, string contentType, string collectionId, string objectId, string movedFromObjectId = null, string ignoreCollectionLock = null, string movedFromCollectionId = null, string changesetId = null)

Create an Object

Creates a new object within the specified collection with optional object metadata. If the object already exists this will result in an error.<br><b>Note: It is highly recommended you use UpdateObject (PUT) instead if you do not care that the object may already exist</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var body = new ObjectRequest(); // ObjectRequest | 
            var contentType = contentType_example;  // string | Content-type of the object to store in the lake
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var movedFromObjectId = movedFromObjectId_example;  // string | The object id this object was moved from (optional) 
            var ignoreCollectionLock = ignoreCollectionLock_example;  // string |  (optional) 
            var movedFromCollectionId = movedFromCollectionId_example;  // string | The collection id this object was moved from (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Create an Object
                ObjectResponse result = apiInstance.CreateObject(body, contentType, collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedFromCollectionId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.CreateObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ObjectRequest**](ObjectRequest.md)|  | 
 **contentType** | **string**| Content-type of the object to store in the lake | 
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **movedFromObjectId** | **string**| The object id this object was moved from | [optional] 
 **ignoreCollectionLock** | **string**|  | [optional] 
 **movedFromCollectionId** | **string**| The collection id this object was moved from | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectResponse**](ObjectResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/xml, image/jpeg, application/atom+xml, application/vnd.visio, image/bmp, image/gif, application/postscript, application/xhtml+xml, video/x-msvideo, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint.presentation.macroenabled.12, audio/wav, text/sgml, application/gzip, application/vnd.ms-excel.sheet.macroenabled.12, application/vnd.ms-excel, audio/basic, application/x-hotdocs-auto, text/plain, application/vnd.ms-word.document.macroenabled.12, image/png, application/octet-stream, application/json, application/msword, application/pdf, application/vnd.ms-powerpoint, audio/aac, audio/mpeg, image/jp2, application/rtf, text/html, video/mpeg, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/tiff, text/csv, application/zip, image/vnd.microsoft.icon, audio/vnd.rn-realaudio
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deleteobject"></a>
# **DeleteObject**
> ObjectResponse DeleteObject (string collectionId, string objectId, string movedToCollectionId = null, string movedToObjectId = null, string changesetId = null)

Delete an Object

Removes the object from the collection with optional object metadata.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var movedToCollectionId = movedToCollectionId_example;  // string | The collection id this collection was moved to (optional) 
            var movedToObjectId = movedToObjectId_example;  // string | The object id this object was moved to (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Delete an Object
                ObjectResponse result = apiInstance.DeleteObject(collectionId, objectId, movedToCollectionId, movedToObjectId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.DeleteObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **movedToCollectionId** | **string**| The collection id this collection was moved to | [optional] 
 **movedToObjectId** | **string**| The object id this object was moved to | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectResponse**](ObjectResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deleteobjects"></a>
# **DeleteObjects**
> RemoveObjectsResponse DeleteObjects (ObjectsBatchDeleteRequest body, string collectionId, string changesetId = null)

Delete a batch of Objects

Removes a batch of objects within the specified collection. Removes are idempotent in that multiple requests will succeed and leave the object in the same removed state. The remove object event notification will only be sent for the first remove request. A count of the unique object IDs found in the request is returned in the response.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var body = new ObjectsBatchDeleteRequest(); // ObjectsBatchDeleteRequest | Objects to delete
            var collectionId = collectionId_example;  // string | The id of a collection
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Delete a batch of Objects
                RemoveObjectsResponse result = apiInstance.DeleteObjects(body, collectionId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.DeleteObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ObjectsBatchDeleteRequest**](ObjectsBatchDeleteRequest.md)| Objects to delete | 
 **collectionId** | **string**| The id of a collection | 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**RemoveObjectsResponse**](RemoveObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="describeobjects"></a>
# **DescribeObjects**
> DescribeObjectsResponse DescribeObjects (string collectionId = null, string maxItems = null, string nextToken = null, string objectId = null, string changesetId = null, string versionNumber = null)

Describe Objects

Returns more details about objects, including S3 Location, version information, and state.  Is limited to 10 objects per response

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DescribeObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection (optional) 
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 
            var objectId = objectId_example;  // string | The id of an object (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 
            var versionNumber = versionNumber_example;  // string | The version number of an object (optional) 

            try
            {
                // Describe Objects
                DescribeObjectsResponse result = apiInstance.DescribeObjects(collectionId, maxItems, nextToken, objectId, changesetId, versionNumber);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.DescribeObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | [optional] 
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 
 **objectId** | **string**| The id of an object | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 
 **versionNumber** | **string**| The version number of an object | [optional] 

### Return type

[**DescribeObjectsResponse**](DescribeObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getobject"></a>
# **GetObject**
> void GetObject (string collectionId, string objectId, string changesetId = null, string versionNumber = null)

Get an Object by ID

Returns an object. If changeset-id is included in the request, then version-number must also be specified.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 
            var versionNumber = versionNumber_example;  // string | The version number of an object (optional) 

            try
            {
                // Get an Object by ID
                apiInstance.GetObject(collectionId, objectId, changesetId, versionNumber);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.GetObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **changesetId** | **string**| The id of a changeset | [optional] 
 **versionNumber** | **string**| The version number of an object | [optional] 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="listobjects"></a>
# **ListObjects**
> ListObjectsResponse ListObjects (string collectionId = null, string state = null, string maxItems = null, string nextToken = null)

List Objects

Returns a list of objects. You can filter down to a collection and/or a particular object-state

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ListObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection (optional) 
            var state = state_example;  // string | Depending on the resource called, state could be of an object or changeset; Allowed values for object: Created and Removed; Allowed values for changeset:  Opened, Closing, Closed, Expired (optional) 
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 

            try
            {
                // List Objects
                ListObjectsResponse result = apiInstance.ListObjects(collectionId, state, maxItems, nextToken);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.ListObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | [optional] 
 **state** | **string**| Depending on the resource called, state could be of an object or changeset; Allowed values for object: Created and Removed; Allowed values for changeset:  Opened, Closing, Closed, Expired | [optional] 
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 

### Return type

[**ListObjectsResponse**](ListObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updateobject"></a>
# **UpdateObject**
> ObjectResponse UpdateObject (ObjectRequest body, string contentType, string collectionId, string objectId, string movedFromObjectId = null, string ignoreCollectionLock = null, string movedToCollectionId = null, string movedToObjectId = null, string movedFromCollectionId = null, string changesetId = null)

Update an Object

Updates an object within the specified collection with optional object metadata. If the object-id does not exist it will create it.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ObjectsApi();
            var body = new ObjectRequest(); // ObjectRequest | 
            var contentType = contentType_example;  // string | Content-type of the object to store in the lake
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var movedFromObjectId = movedFromObjectId_example;  // string | The object id this object was moved from (optional) 
            var ignoreCollectionLock = ignoreCollectionLock_example;  // string |  (optional) 
            var movedToCollectionId = movedToCollectionId_example;  // string | The collection id this collection was moved to (optional) 
            var movedToObjectId = movedToObjectId_example;  // string | The object id this object was moved to (optional) 
            var movedFromCollectionId = movedFromCollectionId_example;  // string | The collection id this object was moved from (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Update an Object
                ObjectResponse result = apiInstance.UpdateObject(body, contentType, collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedToCollectionId, movedToObjectId, movedFromCollectionId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObjectsApi.UpdateObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ObjectRequest**](ObjectRequest.md)|  | 
 **contentType** | **string**| Content-type of the object to store in the lake | 
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **movedFromObjectId** | **string**| The object id this object was moved from | [optional] 
 **ignoreCollectionLock** | **string**|  | [optional] 
 **movedToCollectionId** | **string**| The collection id this collection was moved to | [optional] 
 **movedToObjectId** | **string**| The object id this object was moved to | [optional] 
 **movedFromCollectionId** | **string**| The collection id this object was moved from | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectResponse**](ObjectResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/xml, image/jpeg, application/atom+xml, application/vnd.visio, image/bmp, image/gif, application/postscript, application/xhtml+xml, video/x-msvideo, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint.presentation.macroenabled.12, audio/wav, text/sgml, application/gzip, application/vnd.ms-excel.sheet.macroenabled.12, application/vnd.ms-excel, audio/basic, application/x-hotdocs-auto, text/plain, application/vnd.ms-word.document.macroenabled.12, image/png, application/octet-stream, application/json, application/msword, application/pdf, application/vnd.ms-powerpoint, audio/aac, audio/mpeg, image/jp2, application/rtf, text/html, video/mpeg, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/tiff, text/csv, application/zip, image/vnd.microsoft.icon, audio/vnd.rn-realaudio
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
