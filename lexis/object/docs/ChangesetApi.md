# LexisDataLake.Api.ChangesetApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CloseChangeset**](ChangesetApi.md#closechangeset) | **POST** /changeset/{changesetId} | Close Changeset
[**CreateChangeset**](ChangesetApi.md#createchangeset) | **POST** /changeset | Open Changeset
[**DescribeChangesets**](ChangesetApi.md#describechangesets) | **GET** /changeset/describe | Describe Changesets
[**GetChangeset**](ChangesetApi.md#getchangeset) | **GET** /changeset/{changesetId} | Get a Changeset by ID

<a name="closechangeset"></a>
# **CloseChangeset**
> ChangesetResponse CloseChangeset (CloseChangesetRequest body, string changesetId)

Close Changeset

Close an existing changeset

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CloseChangesetExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ChangesetApi();
            var body = new CloseChangesetRequest(); // CloseChangesetRequest | Changeset metadata to attach to changeset
            var changesetId = changesetId_example;  // string | The id of a changeset

            try
            {
                // Close Changeset
                ChangesetResponse result = apiInstance.CloseChangeset(body, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ChangesetApi.CloseChangeset: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CloseChangesetRequest**](CloseChangesetRequest.md)| Changeset metadata to attach to changeset | 
 **changesetId** | **string**| The id of a changeset | 

### Return type

[**ChangesetResponse**](ChangesetResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="createchangeset"></a>
# **CreateChangeset**
> ChangesetResponse CreateChangeset (string description = null)

Open Changeset

Open new changeset

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateChangesetExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ChangesetApi();
            var description = description_example;  // string | The description of the resource (optional) 

            try
            {
                // Open Changeset
                ChangesetResponse result = apiInstance.CreateChangeset(description);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ChangesetApi.CreateChangeset: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **description** | **string**| The description of the resource | [optional] 

### Return type

[**ChangesetResponse**](ChangesetResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="describechangesets"></a>
# **DescribeChangesets**
> DescribeChangesetsResponse DescribeChangesets (string state = null, string maxItems = null, string ownerId = null, string nextToken = null, string changesetId = null)

Describe Changesets

Returns properties of a list of changesets, including a list of collection-ids and object count in each collection

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DescribeChangesetsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ChangesetApi();
            var state = state_example;  // string | Depending on the resource called, state could be of an object or changeset; Allowed values for object: Created and Removed; Allowed values for changeset:  Opened, Closing, Closed, Expired (optional) 
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var ownerId = ownerId_example;  // string |  (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Describe Changesets
                DescribeChangesetsResponse result = apiInstance.DescribeChangesets(state, maxItems, ownerId, nextToken, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ChangesetApi.DescribeChangesets: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **string**| Depending on the resource called, state could be of an object or changeset; Allowed values for object: Created and Removed; Allowed values for changeset:  Opened, Closing, Closed, Expired | [optional] 
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **ownerId** | **string**|  | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**DescribeChangesetsResponse**](DescribeChangesetsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getchangeset"></a>
# **GetChangeset**
> ChangesetResponse GetChangeset (string changesetId)

Get a Changeset by ID

Returns a changeset details

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetChangesetExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new ChangesetApi();
            var changesetId = changesetId_example;  // string | The id of a changeset

            try
            {
                // Get a Changeset by ID
                ChangesetResponse result = apiInstance.GetChangeset(changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ChangesetApi.GetChangeset: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changesetId** | **string**| The id of a changeset | 

### Return type

[**ChangesetResponse**](ChangesetResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
