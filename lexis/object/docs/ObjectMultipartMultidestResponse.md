# LexisDataLake.Model.ObjectMultipartMultidestResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MultipartUpload** | [**ObjectMultipartUploadProperties**](ObjectMultipartUploadProperties.md) |  | [optional] 
**Objects** | [**List&lt;ObjectProperties&gt;**](ObjectProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

