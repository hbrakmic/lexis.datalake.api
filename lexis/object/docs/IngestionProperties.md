# LexisDataLake.Model.IngestionProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IgnoreCollectionLock** | **bool?** |  | [optional] 
**ChangesetId** | **string** |  | [optional] 
**OwnerId** | **int?** |  | 
**IngestionUrl** | **string** |  | 
**ArchiveFormat** | **string** |  | 
**CollectionId** | **string** |  | 
**CollectionUrl** | **string** |  | 
**IngestionId** | **string** |  | 
**IngestionExpirationDate** | **DateTime?** |  | 
**Description** | **string** |  | [optional] 
**IngestionState** | **string** |  | 
**IngestionTimestamp** | **DateTime?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

