# LexisDataLake.Model.ObjectFolderUploadResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**FolderUpload** | [**ObjectFolderUploadProperties**](ObjectFolderUploadProperties.md) |  | 
**Object** | [**ObjectProperties**](ObjectProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

