# LexisDataLake.Model.ObjectFolderUploadPropertiesCredentials
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SessionToken** | **string** |  | [optional] 
**AccessKeyId** | **string** |  | [optional] 
**SecretAccessKey** | **string** |  | [optional] 
**Expiration** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

