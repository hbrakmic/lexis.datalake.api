# LexisDataLake.Model.RemoveObjectsResponseBatch
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChangesetId** | **string** |  | [optional] 
**OwnerId** | **int?** |  | 
**ObjectCount** | **int?** |  | 
**CollectionId** | **string** |  | 
**CollectionUrl** | **string** |  | 
**AssetId** | **int?** |  | 
**Description** | **string** |  | [optional] 
**RemoveTimestamp** | **DateTime?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

