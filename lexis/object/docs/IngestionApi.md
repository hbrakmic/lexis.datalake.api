# LexisDataLake.Api.IngestionApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CancelIngestion**](IngestionApi.md#cancelingestion) | **DELETE** /ingestion/{ingestionId} | Cancel an Ingestion that is in Pending or Error state
[**CreateIngestion**](IngestionApi.md#createingestion) | **POST** /ingestion/zip | Begin an Ingestion
[**GetIngestion**](IngestionApi.md#getingestion) | **GET** /ingestion/{ingestionId} | Get an Ingestion by ID

<a name="cancelingestion"></a>
# **CancelIngestion**
> CancelIngestionResponse CancelIngestion (string ingestionId)

Cancel an Ingestion that is in Pending or Error state

Cancels an ingestion transaction

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CancelIngestionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new IngestionApi();
            var ingestionId = ingestionId_example;  // string | The id of an ingestion

            try
            {
                // Cancel an Ingestion that is in Pending or Error state
                CancelIngestionResponse result = apiInstance.CancelIngestion(ingestionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling IngestionApi.CancelIngestion: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ingestionId** | **string**| The id of an ingestion | 

### Return type

[**CancelIngestionResponse**](CancelIngestionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="createingestion"></a>
# **CreateIngestion**
> CreateIngestionResponse CreateIngestion (string collectionId, string storedContentType, string ignoreCollectionLock = null, string description = null, string changesetId = null)

Begin an Ingestion

Starts a multipart s3 upload of a zip ingestion file for the collection specified. Each file in the zip archive must be non-encrypted and can be uncompressed or compressed (deflate compression option only). The file names in the zip archive correspond to object ids in the specified collection and cannot be hierarchical. There is a maximum limit of 1.5 million files in the zip file. Uncompressed files cannot be larger than 512MB (contact Data Lake team if you require larger files). You are required to upload the parts and complete the multipart upload request with amazon https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadUploadPart.html, https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadComplete.html

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateIngestionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new IngestionApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var storedContentType = storedContentType_example;  // string | The Content-Type of the object you will be storing in the datalake
            var ignoreCollectionLock = ignoreCollectionLock_example;  // string |  (optional) 
            var description = description_example;  // string | The description of the resource (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Begin an Ingestion
                CreateIngestionResponse result = apiInstance.CreateIngestion(collectionId, storedContentType, ignoreCollectionLock, description, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling IngestionApi.CreateIngestion: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **storedContentType** | **string**| The Content-Type of the object you will be storing in the datalake | 
 **ignoreCollectionLock** | **string**|  | [optional] 
 **description** | **string**| The description of the resource | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**CreateIngestionResponse**](CreateIngestionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getingestion"></a>
# **GetIngestion**
> GetIngestionResponse GetIngestion (string ingestionId)

Get an Ingestion by ID

Returns an ingestion transaction details

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetIngestionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new IngestionApi();
            var ingestionId = ingestionId_example;  // string | The id of an ingestion

            try
            {
                // Get an Ingestion by ID
                GetIngestionResponse result = apiInstance.GetIngestion(ingestionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling IngestionApi.GetIngestion: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ingestionId** | **string**| The id of an ingestion | 

### Return type

[**GetIngestionResponse**](GetIngestionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
