# LexisDataLake.Model.ObjectFolderUploadProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S3** | [**ObjectFolderUploadPropertiesS3**](ObjectFolderUploadPropertiesS3.md) |  | 
**TransactionId** | **string** |  | 
**Credentials** | [**ObjectFolderUploadPropertiesCredentials**](ObjectFolderUploadPropertiesCredentials.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

