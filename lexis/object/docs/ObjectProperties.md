# LexisDataLake.Model.ObjectProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ObjectExpirationDate** | **DateTime?** |  | [optional] 
**ObjectUrl** | **string** |  | 
**TemporaryObjectKeyUrl** | **string** |  | 
**CollectionId** | **string** |  | 
**ExtendedObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**Relocation** | **Object** |  | [optional] 
**ChangesetId** | **string** |  | [optional] 
**ObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**ObjectState** | **string** |  | 
**OwnerId** | **int?** |  | 
**CollectionUrl** | **string** |  | 
**AssetId** | **int?** |  | 
**ChangesetObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**ObjectId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

