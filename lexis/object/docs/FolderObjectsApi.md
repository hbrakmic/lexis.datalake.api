# LexisDataLake.Api.FolderObjectsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CompleteFolder**](FolderObjectsApi.md#completefolder) | **POST** /{objectId}/folder/{transactionId}/complete | Complete a Folder transaction
[**CreateFolder**](FolderObjectsApi.md#createfolder) | **POST** /{objectId}/folder | Begin a Folder create transaction
[**UpdateFolder**](FolderObjectsApi.md#updatefolder) | **PUT** /{objectId}/folder | Begin a Folder update transaction

<a name="completefolder"></a>
# **CompleteFolder**
> ObjectResponse CompleteFolder (string transactionId, string objectId)

Complete a Folder transaction

Ends the transaction of a folder upload and begins processing to place the folder object into the DataLake

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CompleteFolderExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new FolderObjectsApi();
            var transactionId = transactionId_example;  // string | The transaction id of a folder operation in process
            var objectId = objectId_example;  // string | The id of an object

            try
            {
                // Complete a Folder transaction
                ObjectResponse result = apiInstance.CompleteFolder(transactionId, objectId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FolderObjectsApi.CompleteFolder: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transactionId** | **string**| The transaction id of a folder operation in process | 
 **objectId** | **string**| The id of an object | 

### Return type

[**ObjectResponse**](ObjectResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="createfolder"></a>
# **CreateFolder**
> ObjectFolderUploadResponse CreateFolder (string collectionId, string objectId, string changesetId = null)

Begin a Folder create transaction

Begins the transaction to place a new folder object. Returns a staging s3 bucket location you put content into

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateFolderExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new FolderObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Begin a Folder create transaction
                ObjectFolderUploadResponse result = apiInstance.CreateFolder(collectionId, objectId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FolderObjectsApi.CreateFolder: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectFolderUploadResponse**](ObjectFolderUploadResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updatefolder"></a>
# **UpdateFolder**
> ObjectFolderUploadResponse UpdateFolder (string collectionId, string objectId, string changesetId = null)

Begin a Folder update transaction

Begins the transaction to update a folder object. Returns a staging s3 bucket location you put content into

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateFolderExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new FolderObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Begin a Folder update transaction
                ObjectFolderUploadResponse result = apiInstance.UpdateFolder(collectionId, objectId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FolderObjectsApi.UpdateFolder: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectFolderUploadResponse**](ObjectFolderUploadResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
