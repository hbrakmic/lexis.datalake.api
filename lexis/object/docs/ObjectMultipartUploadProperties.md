# LexisDataLake.Model.ObjectMultipartUploadProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S3** | [**ListObjectsResponseS3**](ListObjectsResponseS3.md) |  | 
**UploadId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

