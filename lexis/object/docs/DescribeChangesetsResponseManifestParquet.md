# LexisDataLake.Model.DescribeChangesetsResponseManifestParquet
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**V1** | [**DescribeChangesetsResponseManifestParquetV1**](DescribeChangesetsResponseManifestParquetV1.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

