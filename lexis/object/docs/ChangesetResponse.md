# LexisDataLake.Model.ChangesetResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Changeset** | [**ChangesetProperties**](ChangesetProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

