# LexisDataLake.Api.MultipartObjectsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMultipartObject**](MultipartObjectsApi.md#createmultipartobject) | **POST** /{objectId}/multipart | Create a Multipart Object
[**UpdateMultipartObject**](MultipartObjectsApi.md#updatemultipartobject) | **PUT** /{objectId}/multipart | Update a Multipart Object

<a name="createmultipartobject"></a>
# **CreateMultipartObject**
> ObjectMultipartUploadResponse CreateMultipartObject (string collectionId, string objectId, string movedFromObjectId = null, string ignoreCollectionLock = null, string movedFromCollectionId = null, string storedContentType = null, string changesetId = null)

Create a Multipart Object

Begins the transaction to create an object up to 5 TB in size using S3 Multipart Uploads. Creates the object within the specified collection with optional object metadata. You are required to upload the parts and complete the request with amazon https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadUploadPart.html, https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadComplete.html<br><b>Note: It is highly recommended that you use UpdateMultipartObject (PUT) if you do not care that the object may or may not already exist</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateMultipartObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new MultipartObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var movedFromObjectId = movedFromObjectId_example;  // string | The object id this object was moved from (optional) 
            var ignoreCollectionLock = ignoreCollectionLock_example;  // string |  (optional) 
            var movedFromCollectionId = movedFromCollectionId_example;  // string | The collection id this object was moved from (optional) 
            var storedContentType = storedContentType_example;  // string | The Content-Type of the object you will be storing in the datalake (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Create a Multipart Object
                ObjectMultipartUploadResponse result = apiInstance.CreateMultipartObject(collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedFromCollectionId, storedContentType, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MultipartObjectsApi.CreateMultipartObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **movedFromObjectId** | **string**| The object id this object was moved from | [optional] 
 **ignoreCollectionLock** | **string**|  | [optional] 
 **movedFromCollectionId** | **string**| The collection id this object was moved from | [optional] 
 **storedContentType** | **string**| The Content-Type of the object you will be storing in the datalake | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectMultipartUploadResponse**](ObjectMultipartUploadResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updatemultipartobject"></a>
# **UpdateMultipartObject**
> ObjectMultipartUploadResponse UpdateMultipartObject (string collectionId, string objectId, string movedFromObjectId = null, string ignoreCollectionLock = null, string movedToCollectionId = null, string movedToObjectId = null, string movedFromCollectionId = null, string storedContentType = null, string changesetId = null)

Update a Multipart Object

Begins the transaction to update an object up to 5 TB in size using S3 Multipart Uploads. Updates the object within the specified collection with optional object metadata. If the object-id does not exist it will create it. You are required to upload the parts and complete the request with amazon https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadUploadPart.html, https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadComplete.html<br><b>Note: If the Object does not already exist, this will create it.  The behavior is essentially create or update</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateMultipartObjectExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new MultipartObjectsApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var objectId = objectId_example;  // string | The id of an object
            var movedFromObjectId = movedFromObjectId_example;  // string | The object id this object was moved from (optional) 
            var ignoreCollectionLock = ignoreCollectionLock_example;  // string |  (optional) 
            var movedToCollectionId = movedToCollectionId_example;  // string | The collection id this collection was moved to (optional) 
            var movedToObjectId = movedToObjectId_example;  // string | The object id this object was moved to (optional) 
            var movedFromCollectionId = movedFromCollectionId_example;  // string | The collection id this object was moved from (optional) 
            var storedContentType = storedContentType_example;  // string | The Content-Type of the object you will be storing in the datalake (optional) 
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Update a Multipart Object
                ObjectMultipartUploadResponse result = apiInstance.UpdateMultipartObject(collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedToCollectionId, movedToObjectId, movedFromCollectionId, storedContentType, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MultipartObjectsApi.UpdateMultipartObject: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **objectId** | **string**| The id of an object | 
 **movedFromObjectId** | **string**| The object id this object was moved from | [optional] 
 **ignoreCollectionLock** | **string**|  | [optional] 
 **movedToCollectionId** | **string**| The collection id this collection was moved to | [optional] 
 **movedToObjectId** | **string**| The object id this object was moved to | [optional] 
 **movedFromCollectionId** | **string**| The collection id this object was moved from | [optional] 
 **storedContentType** | **string**| The Content-Type of the object you will be storing in the datalake | [optional] 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectMultipartUploadResponse**](ObjectMultipartUploadResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
