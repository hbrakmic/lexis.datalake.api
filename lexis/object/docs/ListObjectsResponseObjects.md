# LexisDataLake.Model.ListObjectsResponseObjects
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S3** | [**ListObjectsResponseS3**](ListObjectsResponseS3.md) |  | [optional] 
**RawContentLength** | **int?** |  | [optional] 
**ObjectState** | **string** |  | 
**CollectionId** | **string** |  | 
**ObjectId** | **string** |  | 
**ObjectKeyUrl** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

