# LexisDataLake.Api.MultiDestinationApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UpdateObjectMultidest**](MultiDestinationApi.md#updateobjectmultidest) | **PUT** /{objectId}/multidest | Update a single Object to as many as ten collections in one call
[**UpdateObjectMultipartMultidest**](MultiDestinationApi.md#updateobjectmultipartmultidest) | **PUT** /{objectId}/multidest/multipart | Update a Multipart Object to as many as ten collections in one call

<a name="updateobjectmultidest"></a>
# **UpdateObjectMultidest**
> ObjectMultidestResponse UpdateObjectMultidest (ObjectRequest body, string contentType, string collectionIds, string objectId, string changesetId = null)

Update a single Object to as many as ten collections in one call

Updates an object to up to ten specified collections with optional object metadata. If the object-id does not exist it will create it.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateObjectMultidestExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new MultiDestinationApi();
            var body = new ObjectRequest(); // ObjectRequest | 
            var contentType = contentType_example;  // string | Content-type of the object to store in the lake
            var collectionIds = collectionIds_example;  // string | Comma separated list of collection ids, maximum of 10
            var objectId = objectId_example;  // string | The id of an object
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Update a single Object to as many as ten collections in one call
                ObjectMultidestResponse result = apiInstance.UpdateObjectMultidest(body, contentType, collectionIds, objectId, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MultiDestinationApi.UpdateObjectMultidest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ObjectRequest**](ObjectRequest.md)|  | 
 **contentType** | **string**| Content-type of the object to store in the lake | 
 **collectionIds** | **string**| Comma separated list of collection ids, maximum of 10 | 
 **objectId** | **string**| The id of an object | 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectMultidestResponse**](ObjectMultidestResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/xml, image/jpeg, application/atom+xml, application/vnd.visio, image/bmp, image/gif, application/postscript, application/xhtml+xml, video/x-msvideo, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint.presentation.macroenabled.12, audio/wav, text/sgml, application/gzip, application/vnd.ms-excel.sheet.macroenabled.12, application/vnd.ms-excel, audio/basic, application/x-hotdocs-auto, text/plain, application/vnd.ms-word.document.macroenabled.12, image/png, application/octet-stream, application/json, application/msword, application/pdf, application/vnd.ms-powerpoint, audio/aac, audio/mpeg, image/jp2, application/rtf, text/html, video/mpeg, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/tiff, text/csv, application/zip, image/vnd.microsoft.icon, audio/vnd.rn-realaudio
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updateobjectmultipartmultidest"></a>
# **UpdateObjectMultipartMultidest**
> ObjectMultipartMultidestResponse UpdateObjectMultipartMultidest (string objectId, string storedContentType, string collectionIds, string changesetId = null)

Update a Multipart Object to as many as ten collections in one call

Begins the transaction to update an object up to 5 TB in size using S3 Multipart Uploads to as many as ten collections. Updates the object within the specified collection with optional object metadata. You are required to upload the parts and complete the request with amazon https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadUploadPart.html, https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadComplete.html<br><b>Note: If the Object does not already exist, this will create it.  The behavior is essentially create or update</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateObjectMultipartMultidestExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new MultiDestinationApi();
            var objectId = objectId_example;  // string | The id of an object
            var storedContentType = storedContentType_example;  // string | The Content-Type of the object you will be storing in the datalake
            var collectionIds = collectionIds_example;  // string | Comma separated list of collection ids, maximum of 10
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 

            try
            {
                // Update a Multipart Object to as many as ten collections in one call
                ObjectMultipartMultidestResponse result = apiInstance.UpdateObjectMultipartMultidest(objectId, storedContentType, collectionIds, changesetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MultiDestinationApi.UpdateObjectMultipartMultidest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectId** | **string**| The id of an object | 
 **storedContentType** | **string**| The Content-Type of the object you will be storing in the datalake | 
 **collectionIds** | **string**| Comma separated list of collection ids, maximum of 10 | 
 **changesetId** | **string**| The id of a changeset | [optional] 

### Return type

[**ObjectMultipartMultidestResponse**](ObjectMultipartMultidestResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
