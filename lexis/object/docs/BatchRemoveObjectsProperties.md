# LexisDataLake.Model.BatchRemoveObjectsProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChangesetId** | **string** |  | [optional] 
**BatchRemoveTimestamp** | **DateTime?** |  | 
**BatchRemoveExpirationDate** | **DateTime?** |  | 
**OwnerId** | **int?** |  | 
**BatchRemoveUrl** | **string** |  | 
**CollectionId** | **string** |  | 
**CollectionUrl** | **string** |  | 
**Description** | **string** |  | [optional] 
**BatchRemoveId** | **string** |  | 
**BatchRemoveState** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

