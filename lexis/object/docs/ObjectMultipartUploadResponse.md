# LexisDataLake.Model.ObjectMultipartUploadResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MultipartUpload** | [**ObjectMultipartUploadProperties**](ObjectMultipartUploadProperties.md) |  | [optional] 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Object** | [**ObjectProperties**](ObjectProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

