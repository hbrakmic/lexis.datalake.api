# LexisDataLake.Model.GetIngestionProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IgnoreCollectionLock** | **bool?** |  | [optional] 
**ObjectTrackedCount** | **int?** |  | [optional] 
**ObjectErrorCount** | **int?** |  | [optional] 
**CollectionId** | **string** |  | 
**IngestionId** | **string** |  | 
**Description** | **string** |  | [optional] 
**IngestionTimestamp** | **DateTime?** |  | 
**ErrorCode** | **string** |  | [optional] 
**ObjectUpdatedCount** | **int?** |  | [optional] 
**ChangesetId** | **string** |  | [optional] 
**IngestionUrl** | **string** |  | 
**OwnerId** | **int?** |  | 
**ArchiveFormat** | **string** |  | 
**ObjectCount** | **int?** |  | [optional] 
**ErrorDescription** | **string** |  | [optional] 
**CollectionUrl** | **string** |  | 
**IngestionExpirationDate** | **DateTime?** |  | 
**IngestionState** | **string** |  | 
**ObjectProcessedCount** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

