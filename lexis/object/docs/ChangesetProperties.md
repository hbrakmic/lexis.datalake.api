# LexisDataLake.Model.ChangesetProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChangesetId** | **string** |  | 
**ChangesetMetadata** | **Object** |  | [optional] 
**ChangesetTimestamp** | **DateTime?** |  | 
**OwnerId** | **int?** |  | 
**Manifest** | [**DescribeChangesetsResponseManifest**](DescribeChangesetsResponseManifest.md) |  | [optional] 
**ChangesetClosedTimestamp** | **DateTime?** |  | [optional] 
**Description** | **string** |  | [optional] 
**ChangesetExpirationDate** | **DateTime?** |  | 
**ChangesetState** | **string** |  | 
**ChangesetUrl** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

