# LexisDataLake.Api.BatchRemoveApi

All URIs are relative to *https://datalake.content.aws.lexis.com/objects/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BatchRemoveObjects**](BatchRemoveApi.md#batchremoveobjects) | **POST** /batch/remove | Batch Remove Objects Multipart Upload
[**CancelBatchRemoveObjects**](BatchRemoveApi.md#cancelbatchremoveobjects) | **DELETE** /batch/remove/{batchRemoveId} | Cancel a Batch Remove Objects that is in Pending or Error state
[**GetBatchRemoveObjects**](BatchRemoveApi.md#getbatchremoveobjects) | **GET** /batch/remove/{batchRemoveId} | Get a BatchRemove by ID

<a name="batchremoveobjects"></a>
# **BatchRemoveObjects**
> CreateBatchRemoveObjectsResponse BatchRemoveObjects (string collectionId, string changesetId = null, string description = null)

Batch Remove Objects Multipart Upload

Starts a multipart s3 upload for BatchRemoveObjects. You are required to upload the parts and complete the multipart upload request with amazon https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadUploadPart.html, https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadComplete.html . The payload uploaded should be a newline delimited json file http://ndjson.org/

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class BatchRemoveObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new BatchRemoveApi();
            var collectionId = collectionId_example;  // string | The id of a collection
            var changesetId = changesetId_example;  // string | The id of a changeset (optional) 
            var description = description_example;  // string | The description of the resource (optional) 

            try
            {
                // Batch Remove Objects Multipart Upload
                CreateBatchRemoveObjectsResponse result = apiInstance.BatchRemoveObjects(collectionId, changesetId, description);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BatchRemoveApi.BatchRemoveObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 
 **changesetId** | **string**| The id of a changeset | [optional] 
 **description** | **string**| The description of the resource | [optional] 

### Return type

[**CreateBatchRemoveObjectsResponse**](CreateBatchRemoveObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="cancelbatchremoveobjects"></a>
# **CancelBatchRemoveObjects**
> CancelBatchRemoveObjectsResponse CancelBatchRemoveObjects (string batchRemoveId)

Cancel a Batch Remove Objects that is in Pending or Error state

Cancels a Batch Remove Objects transaction

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CancelBatchRemoveObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new BatchRemoveApi();
            var batchRemoveId = batchRemoveId_example;  // string | The id of an batch remove

            try
            {
                // Cancel a Batch Remove Objects that is in Pending or Error state
                CancelBatchRemoveObjectsResponse result = apiInstance.CancelBatchRemoveObjects(batchRemoveId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BatchRemoveApi.CancelBatchRemoveObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchRemoveId** | **string**| The id of an batch remove | 

### Return type

[**CancelBatchRemoveObjectsResponse**](CancelBatchRemoveObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getbatchremoveobjects"></a>
# **GetBatchRemoveObjects**
> GetBatchRemoveObjectsResponse GetBatchRemoveObjects (string batchRemoveId)

Get a BatchRemove by ID

Returns batch-remove details

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetBatchRemoveObjectsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new BatchRemoveApi();
            var batchRemoveId = batchRemoveId_example;  // string | The id of an batch remove

            try
            {
                // Get a BatchRemove by ID
                GetBatchRemoveObjectsResponse result = apiInstance.GetBatchRemoveObjects(batchRemoveId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BatchRemoveApi.GetBatchRemoveObjects: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchRemoveId** | **string**| The id of an batch remove | 

### Return type

[**GetBatchRemoveObjectsResponse**](GetBatchRemoveObjectsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
