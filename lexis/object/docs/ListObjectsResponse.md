# LexisDataLake.Model.ListObjectsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextToken** | **string** |  | [optional] 
**ItemCount** | **int?** |  | 
**Objects** | [**List&lt;ListObjectsResponseObjects&gt;**](ListObjectsResponseObjects.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

