# LexisDataLake.Model.DescribeObjectsResponseObjects
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S3** | [**ListObjectsResponseS3**](ListObjectsResponseS3.md) |  | [optional] 
**ObjectExpirationDate** | **DateTime?** |  | [optional] 
**ObjectUrl** | **string** |  | 
**CollectionId** | **string** |  | 
**VersionTimestamp** | **DateTime?** |  | [optional] 
**ExtendedObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**Relocation** | **Object** |  | [optional] 
**RawContentLength** | **int?** |  | [optional] 
**ChangesetId** | **string** |  | [optional] 
**ObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**ObjectState** | **string** |  | 
**CollectionUrl** | **string** |  | 
**ContentType** | **string** |  | [optional] 
**ChangesetObjectMetadata** | **Dictionary&lt;string, string&gt;** |  | [optional] 
**ObjectId** | **string** |  | 
**VersionNumber** | **int?** |  | [optional] 
**ObjectKeyUrl** | **string** |  | [optional] 
**ReplicatedBuckets** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

