/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = LexisDataLake.Client.SwaggerDateConverter;

namespace LexisDataLake.Model
{
    /// <summary>
    /// ErrorContextProperties
    /// </summary>
    [DataContract]
        public partial class ErrorContextProperties :  IEquatable<ErrorContextProperties>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorContextProperties" /> class.
        /// </summary>
        /// <param name="stage">stage (required).</param>
        /// <param name="requestTimeEpoch">requestTimeEpoch (required).</param>
        /// <param name="requestId">requestId (required).</param>
        /// <param name="apiId">apiId (required).</param>
        /// <param name="resourceId">resourceId (required).</param>
        public ErrorContextProperties(string stage = default(string), string requestTimeEpoch = default(string), string requestId = default(string), string apiId = default(string), string resourceId = default(string))
        {
            // to ensure "stage" is required (not null)
            if (stage == null)
            {
                throw new InvalidDataException("stage is a required property for ErrorContextProperties and cannot be null");
            }
            else
            {
                this.Stage = stage;
            }
            // to ensure "requestTimeEpoch" is required (not null)
            if (requestTimeEpoch == null)
            {
                throw new InvalidDataException("requestTimeEpoch is a required property for ErrorContextProperties and cannot be null");
            }
            else
            {
                this.RequestTimeEpoch = requestTimeEpoch;
            }
            // to ensure "requestId" is required (not null)
            if (requestId == null)
            {
                throw new InvalidDataException("requestId is a required property for ErrorContextProperties and cannot be null");
            }
            else
            {
                this.RequestId = requestId;
            }
            // to ensure "apiId" is required (not null)
            if (apiId == null)
            {
                throw new InvalidDataException("apiId is a required property for ErrorContextProperties and cannot be null");
            }
            else
            {
                this.ApiId = apiId;
            }
            // to ensure "resourceId" is required (not null)
            if (resourceId == null)
            {
                throw new InvalidDataException("resourceId is a required property for ErrorContextProperties and cannot be null");
            }
            else
            {
                this.ResourceId = resourceId;
            }
        }
        
        /// <summary>
        /// Gets or Sets Stage
        /// </summary>
        [DataMember(Name="stage", EmitDefaultValue=false)]
        public string Stage { get; set; }

        /// <summary>
        /// Gets or Sets RequestTimeEpoch
        /// </summary>
        [DataMember(Name="request-time-epoch", EmitDefaultValue=false)]
        public string RequestTimeEpoch { get; set; }

        /// <summary>
        /// Gets or Sets RequestId
        /// </summary>
        [DataMember(Name="request-id", EmitDefaultValue=false)]
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or Sets ApiId
        /// </summary>
        [DataMember(Name="api-id", EmitDefaultValue=false)]
        public string ApiId { get; set; }

        /// <summary>
        /// Gets or Sets ResourceId
        /// </summary>
        [DataMember(Name="resource-id", EmitDefaultValue=false)]
        public string ResourceId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ErrorContextProperties {\n");
            sb.Append("  Stage: ").Append(Stage).Append("\n");
            sb.Append("  RequestTimeEpoch: ").Append(RequestTimeEpoch).Append("\n");
            sb.Append("  RequestId: ").Append(RequestId).Append("\n");
            sb.Append("  ApiId: ").Append(ApiId).Append("\n");
            sb.Append("  ResourceId: ").Append(ResourceId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ErrorContextProperties);
        }

        /// <summary>
        /// Returns true if ErrorContextProperties instances are equal
        /// </summary>
        /// <param name="input">Instance of ErrorContextProperties to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ErrorContextProperties input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Stage == input.Stage ||
                    (this.Stage != null &&
                    this.Stage.Equals(input.Stage))
                ) && 
                (
                    this.RequestTimeEpoch == input.RequestTimeEpoch ||
                    (this.RequestTimeEpoch != null &&
                    this.RequestTimeEpoch.Equals(input.RequestTimeEpoch))
                ) && 
                (
                    this.RequestId == input.RequestId ||
                    (this.RequestId != null &&
                    this.RequestId.Equals(input.RequestId))
                ) && 
                (
                    this.ApiId == input.ApiId ||
                    (this.ApiId != null &&
                    this.ApiId.Equals(input.ApiId))
                ) && 
                (
                    this.ResourceId == input.ResourceId ||
                    (this.ResourceId != null &&
                    this.ResourceId.Equals(input.ResourceId))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Stage != null)
                    hashCode = hashCode * 59 + this.Stage.GetHashCode();
                if (this.RequestTimeEpoch != null)
                    hashCode = hashCode * 59 + this.RequestTimeEpoch.GetHashCode();
                if (this.RequestId != null)
                    hashCode = hashCode * 59 + this.RequestId.GetHashCode();
                if (this.ApiId != null)
                    hashCode = hashCode * 59 + this.ApiId.GetHashCode();
                if (this.ResourceId != null)
                    hashCode = hashCode * 59 + this.ResourceId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
