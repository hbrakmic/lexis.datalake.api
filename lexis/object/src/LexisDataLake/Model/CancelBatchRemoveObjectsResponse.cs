/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = LexisDataLake.Client.SwaggerDateConverter;

namespace LexisDataLake.Model
{
    /// <summary>
    /// CancelBatchRemoveObjectsResponse
    /// </summary>
    [DataContract]
        public partial class CancelBatchRemoveObjectsResponse :  IEquatable<CancelBatchRemoveObjectsResponse>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CancelBatchRemoveObjectsResponse" /> class.
        /// </summary>
        /// <param name="batchRemove">batchRemove (required).</param>
        /// <param name="context">context (required).</param>
        public CancelBatchRemoveObjectsResponse(BatchRemoveObjectsProperties batchRemove = default(BatchRemoveObjectsProperties), ContextProperties context = default(ContextProperties))
        {
            // to ensure "batchRemove" is required (not null)
            if (batchRemove == null)
            {
                throw new InvalidDataException("batchRemove is a required property for CancelBatchRemoveObjectsResponse and cannot be null");
            }
            else
            {
                this.BatchRemove = batchRemove;
            }
            // to ensure "context" is required (not null)
            if (context == null)
            {
                throw new InvalidDataException("context is a required property for CancelBatchRemoveObjectsResponse and cannot be null");
            }
            else
            {
                this.Context = context;
            }
        }
        
        /// <summary>
        /// Gets or Sets BatchRemove
        /// </summary>
        [DataMember(Name="batch-remove", EmitDefaultValue=false)]
        public BatchRemoveObjectsProperties BatchRemove { get; set; }

        /// <summary>
        /// Gets or Sets Context
        /// </summary>
        [DataMember(Name="context", EmitDefaultValue=false)]
        public ContextProperties Context { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CancelBatchRemoveObjectsResponse {\n");
            sb.Append("  BatchRemove: ").Append(BatchRemove).Append("\n");
            sb.Append("  Context: ").Append(Context).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as CancelBatchRemoveObjectsResponse);
        }

        /// <summary>
        /// Returns true if CancelBatchRemoveObjectsResponse instances are equal
        /// </summary>
        /// <param name="input">Instance of CancelBatchRemoveObjectsResponse to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CancelBatchRemoveObjectsResponse input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BatchRemove == input.BatchRemove ||
                    (this.BatchRemove != null &&
                    this.BatchRemove.Equals(input.BatchRemove))
                ) && 
                (
                    this.Context == input.Context ||
                    (this.Context != null &&
                    this.Context.Equals(input.Context))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.BatchRemove != null)
                    hashCode = hashCode * 59 + this.BatchRemove.GetHashCode();
                if (this.Context != null)
                    hashCode = hashCode * 59 + this.Context.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
