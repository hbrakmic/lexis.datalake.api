/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using LexisDataLake.Api;
using LexisDataLake.Model;
using LexisDataLake.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing ListObjectsResponse
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ListObjectsResponseTests
    {
        // TODO uncomment below to declare an instance variable for ListObjectsResponse
        //private ListObjectsResponse instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of ListObjectsResponse
            //instance = new ListObjectsResponse();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ListObjectsResponse
        /// </summary>
        [Test]
        public void ListObjectsResponseInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" ListObjectsResponse
            //Assert.IsInstanceOfType<ListObjectsResponse> (instance, "variable 'instance' is a ListObjectsResponse");
        }


        /// <summary>
        /// Test the property 'NextToken'
        /// </summary>
        [Test]
        public void NextTokenTest()
        {
            // TODO unit test for the property 'NextToken'
        }
        /// <summary>
        /// Test the property 'ItemCount'
        /// </summary>
        [Test]
        public void ItemCountTest()
        {
            // TODO unit test for the property 'ItemCount'
        }
        /// <summary>
        /// Test the property 'Objects'
        /// </summary>
        [Test]
        public void ObjectsTest()
        {
            // TODO unit test for the property 'Objects'
        }
        /// <summary>
        /// Test the property 'Context'
        /// </summary>
        [Test]
        public void ContextTest()
        {
            // TODO unit test for the property 'Context'
        }

    }

}
