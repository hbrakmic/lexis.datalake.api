/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using LexisDataLake.Api;
using LexisDataLake.Model;
using LexisDataLake.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing ChangesetProperties
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ChangesetPropertiesTests
    {
        // TODO uncomment below to declare an instance variable for ChangesetProperties
        //private ChangesetProperties instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of ChangesetProperties
            //instance = new ChangesetProperties();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ChangesetProperties
        /// </summary>
        [Test]
        public void ChangesetPropertiesInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" ChangesetProperties
            //Assert.IsInstanceOfType<ChangesetProperties> (instance, "variable 'instance' is a ChangesetProperties");
        }


        /// <summary>
        /// Test the property 'ChangesetId'
        /// </summary>
        [Test]
        public void ChangesetIdTest()
        {
            // TODO unit test for the property 'ChangesetId'
        }
        /// <summary>
        /// Test the property 'ChangesetMetadata'
        /// </summary>
        [Test]
        public void ChangesetMetadataTest()
        {
            // TODO unit test for the property 'ChangesetMetadata'
        }
        /// <summary>
        /// Test the property 'ChangesetTimestamp'
        /// </summary>
        [Test]
        public void ChangesetTimestampTest()
        {
            // TODO unit test for the property 'ChangesetTimestamp'
        }
        /// <summary>
        /// Test the property 'OwnerId'
        /// </summary>
        [Test]
        public void OwnerIdTest()
        {
            // TODO unit test for the property 'OwnerId'
        }
        /// <summary>
        /// Test the property 'Manifest'
        /// </summary>
        [Test]
        public void ManifestTest()
        {
            // TODO unit test for the property 'Manifest'
        }
        /// <summary>
        /// Test the property 'ChangesetClosedTimestamp'
        /// </summary>
        [Test]
        public void ChangesetClosedTimestampTest()
        {
            // TODO unit test for the property 'ChangesetClosedTimestamp'
        }
        /// <summary>
        /// Test the property 'Description'
        /// </summary>
        [Test]
        public void DescriptionTest()
        {
            // TODO unit test for the property 'Description'
        }
        /// <summary>
        /// Test the property 'ChangesetExpirationDate'
        /// </summary>
        [Test]
        public void ChangesetExpirationDateTest()
        {
            // TODO unit test for the property 'ChangesetExpirationDate'
        }
        /// <summary>
        /// Test the property 'ChangesetState'
        /// </summary>
        [Test]
        public void ChangesetStateTest()
        {
            // TODO unit test for the property 'ChangesetState'
        }
        /// <summary>
        /// Test the property 'ChangesetUrl'
        /// </summary>
        [Test]
        public void ChangesetUrlTest()
        {
            // TODO unit test for the property 'ChangesetUrl'
        }

    }

}
