/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using LexisDataLake.Client;
using LexisDataLake.Api;
using LexisDataLake.Model;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing ObjectsApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class ObjectsApiTests
    {
        private ObjectsApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new ObjectsApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ObjectsApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' ObjectsApi
            //Assert.IsInstanceOfType(typeof(ObjectsApi), instance, "instance is a ObjectsApi");
        }

        /// <summary>
        /// Test CreateObject
        /// </summary>
        [Test]
        public void CreateObjectTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //ObjectRequest body = null;
            //string contentType = null;
            //string collectionId = null;
            //string objectId = null;
            //string movedFromObjectId = null;
            //string ignoreCollectionLock = null;
            //string movedFromCollectionId = null;
            //string changesetId = null;
            //var response = instance.CreateObject(body, contentType, collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedFromCollectionId, changesetId);
            //Assert.IsInstanceOf<ObjectResponse> (response, "response is ObjectResponse");
        }
        /// <summary>
        /// Test DeleteObject
        /// </summary>
        [Test]
        public void DeleteObjectTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string collectionId = null;
            //string objectId = null;
            //string movedToCollectionId = null;
            //string movedToObjectId = null;
            //string changesetId = null;
            //var response = instance.DeleteObject(collectionId, objectId, movedToCollectionId, movedToObjectId, changesetId);
            //Assert.IsInstanceOf<ObjectResponse> (response, "response is ObjectResponse");
        }
        /// <summary>
        /// Test DeleteObjects
        /// </summary>
        [Test]
        public void DeleteObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //ObjectsBatchDeleteRequest body = null;
            //string collectionId = null;
            //string changesetId = null;
            //var response = instance.DeleteObjects(body, collectionId, changesetId);
            //Assert.IsInstanceOf<RemoveObjectsResponse> (response, "response is RemoveObjectsResponse");
        }
        /// <summary>
        /// Test DescribeObjects
        /// </summary>
        [Test]
        public void DescribeObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string collectionId = null;
            //string maxItems = null;
            //string nextToken = null;
            //string objectId = null;
            //string changesetId = null;
            //string versionNumber = null;
            //var response = instance.DescribeObjects(collectionId, maxItems, nextToken, objectId, changesetId, versionNumber);
            //Assert.IsInstanceOf<DescribeObjectsResponse> (response, "response is DescribeObjectsResponse");
        }
        /// <summary>
        /// Test GetObject
        /// </summary>
        [Test]
        public void GetObjectTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string collectionId = null;
            //string objectId = null;
            //string changesetId = null;
            //string versionNumber = null;
            //instance.GetObject(collectionId, objectId, changesetId, versionNumber);
            
        }
        /// <summary>
        /// Test ListObjects
        /// </summary>
        [Test]
        public void ListObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string collectionId = null;
            //string state = null;
            //string maxItems = null;
            //string nextToken = null;
            //var response = instance.ListObjects(collectionId, state, maxItems, nextToken);
            //Assert.IsInstanceOf<ListObjectsResponse> (response, "response is ListObjectsResponse");
        }
        /// <summary>
        /// Test UpdateObject
        /// </summary>
        [Test]
        public void UpdateObjectTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //ObjectRequest body = null;
            //string contentType = null;
            //string collectionId = null;
            //string objectId = null;
            //string movedFromObjectId = null;
            //string ignoreCollectionLock = null;
            //string movedToCollectionId = null;
            //string movedToObjectId = null;
            //string movedFromCollectionId = null;
            //string changesetId = null;
            //var response = instance.UpdateObject(body, contentType, collectionId, objectId, movedFromObjectId, ignoreCollectionLock, movedToCollectionId, movedToObjectId, movedFromCollectionId, changesetId);
            //Assert.IsInstanceOf<ObjectResponse> (response, "response is ObjectResponse");
        }
    }

}
