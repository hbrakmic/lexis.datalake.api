/* 
 * DataLakeApi-release-Object
 *
 * The Object API allows users to create, update, get, or delete Objects in the DataLake.
 *
 * OpenAPI spec version: 2020-12-22T22:13:55Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using LexisDataLake.Client;
using LexisDataLake.Api;
using LexisDataLake.Model;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing BatchRemoveApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class BatchRemoveApiTests
    {
        private BatchRemoveApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new BatchRemoveApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of BatchRemoveApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' BatchRemoveApi
            //Assert.IsInstanceOfType(typeof(BatchRemoveApi), instance, "instance is a BatchRemoveApi");
        }

        /// <summary>
        /// Test BatchRemoveObjects
        /// </summary>
        [Test]
        public void BatchRemoveObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string collectionId = null;
            //string changesetId = null;
            //string description = null;
            //var response = instance.BatchRemoveObjects(collectionId, changesetId, description);
            //Assert.IsInstanceOf<CreateBatchRemoveObjectsResponse> (response, "response is CreateBatchRemoveObjectsResponse");
        }
        /// <summary>
        /// Test CancelBatchRemoveObjects
        /// </summary>
        [Test]
        public void CancelBatchRemoveObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string batchRemoveId = null;
            //var response = instance.CancelBatchRemoveObjects(batchRemoveId);
            //Assert.IsInstanceOf<CancelBatchRemoveObjectsResponse> (response, "response is CancelBatchRemoveObjectsResponse");
        }
        /// <summary>
        /// Test GetBatchRemoveObjects
        /// </summary>
        [Test]
        public void GetBatchRemoveObjectsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string batchRemoveId = null;
            //var response = instance.GetBatchRemoveObjects(batchRemoveId);
            //Assert.IsInstanceOf<GetBatchRemoveObjectsResponse> (response, "response is GetBatchRemoveObjectsResponse");
        }
    }

}
