# LexisDataLake.Model.ListCollectionsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextToken** | **string** |  | [optional] 
**ItemCount** | **int?** |  | 
**Collections** | [**List&lt;CollectionProperties&gt;**](CollectionProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

