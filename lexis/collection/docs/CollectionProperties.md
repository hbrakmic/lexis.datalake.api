# LexisDataLake.Model.CollectionProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ObjectExpirationHours** | **int?** |  | [optional] 
**OwnerId** | **int?** |  | 
**CollectionId** | **string** |  | 
**Restricted** | **bool?** |  | 
**CollectionUrl** | **string** |  | 
**AssetId** | **int?** |  | 
**Description** | **string** |  | [optional] 
**ClassificationType** | **string** |  | 
**RemovedObjectExpirationHours** | **int?** |  | [optional] 
**CollectionState** | **string** |  | 
**CollectionTimestamp** | **DateTime?** |  | 
**OldObjectVersionsToKeep** | **int?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

