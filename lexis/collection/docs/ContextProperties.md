# LexisDataLake.Model.ContextProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Stage** | **string** |  | 
**RequestTime** | **DateTime?** |  | 
**RequestTimeEpoch** | **string** |  | 
**RequestId** | **string** |  | 
**ApiId** | **string** |  | 
**ResourceId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

