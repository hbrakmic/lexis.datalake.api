# LexisDataLake.Api.CollectionsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/collections/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCollection**](CollectionsApi.md#createcollection) | **POST** /{collectionId} | Create a Collection
[**DeleteCollection**](CollectionsApi.md#deletecollection) | **DELETE** /{collectionId} | Delete a Collection
[**GetCollection**](CollectionsApi.md#getcollection) | **GET** /{collectionId} | Get a Collection by ID
[**ListCollections**](CollectionsApi.md#listcollections) | **GET** / | List Collections
[**UpdateCollection**](CollectionsApi.md#updatecollection) | **PATCH** /{collectionId} | Update a Collection

<a name="createcollection"></a>
# **CreateCollection**
> CollectionResponse CreateCollection (CreateCollectionRequest body, string collectionId)

Create a Collection

Creates a new collection with a user specified collection-id. The values for old-object-versions-to-keep and collection-id cannot be changed after a collection is created. old-object-versions-to-keep represents how many previous versions to keep of an object. The value 0 will keep ONLY the current version. 5 will keep current version + 5 previous version, 2 will keep current version + 2 previous version, etc.. -1 will keep all versions.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateCollectionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CollectionsApi();
            var body = new CreateCollectionRequest(); // CreateCollectionRequest | Collection to create
            var collectionId = collectionId_example;  // string | The id of a collection

            try
            {
                // Create a Collection
                CollectionResponse result = apiInstance.CreateCollection(body, collectionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CollectionsApi.CreateCollection: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateCollectionRequest**](CreateCollectionRequest.md)| Collection to create | 
 **collectionId** | **string**| The id of a collection | 

### Return type

[**CollectionResponse**](CollectionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deletecollection"></a>
# **DeleteCollection**
> CollectionResponse DeleteCollection (string collectionId)

Delete a Collection

Deletes a collection. All the collection's objects must be removed beforehand.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteCollectionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CollectionsApi();
            var collectionId = collectionId_example;  // string | The id of a collection

            try
            {
                // Delete a Collection
                CollectionResponse result = apiInstance.DeleteCollection(collectionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CollectionsApi.DeleteCollection: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 

### Return type

[**CollectionResponse**](CollectionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getcollection"></a>
# **GetCollection**
> CollectionResponse GetCollection (string collectionId)

Get a Collection by ID

Returns a single collection

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetCollectionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CollectionsApi();
            var collectionId = collectionId_example;  // string | The id of a collection

            try
            {
                // Get a Collection by ID
                CollectionResponse result = apiInstance.GetCollection(collectionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CollectionsApi.GetCollection: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **collectionId** | **string**| The id of a collection | 

### Return type

[**CollectionResponse**](CollectionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="listcollections"></a>
# **ListCollections**
> ListCollectionsResponse ListCollections (string maxItems = null, string ownerId = null, string nextToken = null)

List Collections

Returns a list of collections. You can filter down to an owner.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ListCollectionsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CollectionsApi();
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var ownerId = ownerId_example;  // string | The id of an owner (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 

            try
            {
                // List Collections
                ListCollectionsResponse result = apiInstance.ListCollections(maxItems, ownerId, nextToken);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CollectionsApi.ListCollections: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **ownerId** | **string**| The id of an owner | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 

### Return type

[**ListCollectionsResponse**](ListCollectionsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updatecollection"></a>
# **UpdateCollection**
> CollectionResponse UpdateCollection (UpdateCollectionRequest body, string collectionId)

Update a Collection

The value of the patch operation is determined by the data type of the path: suspended=bool asset-id=int. The /suspended operation will suspend (value=true) or resume (value=false) a collection

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateCollectionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CollectionsApi();
            var body = new UpdateCollectionRequest(); // UpdateCollectionRequest | Collection to update
            var collectionId = collectionId_example;  // string | The id of a collection

            try
            {
                // Update a Collection
                CollectionResponse result = apiInstance.UpdateCollection(body, collectionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CollectionsApi.UpdateCollection: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateCollectionRequest**](UpdateCollectionRequest.md)| Collection to update | 
 **collectionId** | **string**| The id of a collection | 

### Return type

[**CollectionResponse**](CollectionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
