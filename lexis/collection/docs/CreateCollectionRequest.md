# LexisDataLake.Model.CreateCollectionRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ObjectExpirationHours** | **int?** |  | [optional] 
**Restricted** | **bool?** |  | [optional] 
**AssetId** | **int?** |  | 
**Description** | **string** |  | [optional] 
**ClassificationType** | **string** |  | [optional] 
**RemovedObjectExpirationHours** | **int?** |  | [optional] 
**OldObjectVersionsToKeep** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

