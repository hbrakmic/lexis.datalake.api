# LexisDataLake.Model.UpdateCollectionRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PatchOperations** | [**List&lt;UpdateCollectionRequestPatchoperations&gt;**](UpdateCollectionRequestPatchoperations.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

