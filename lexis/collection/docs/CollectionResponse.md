# LexisDataLake.Model.CollectionResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Collection** | [**CollectionProperties**](CollectionProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

