# LexisDataLake.Model.UpdateCollectionRequestPatchoperations
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Op** | **string** | The operation to be performed | 
**Path** | **string** | The path of the json object to replace | 
**Value** | **Object** | The value to be used within the operations. It&#x27;s type is determined by the path type: suspended&#x3D;bool asset-id&#x3D;int | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

