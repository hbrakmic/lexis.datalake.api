# LexisDataLake.Model.DescribeCatalogsResponseCatalogs
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OwnerId** | **int?** |  | 
**Description** | **string** |  | [optional] 
**Truncated** | **bool?** |  | [optional] 
**CatalogTimestamp** | **string** |  | 
**CatalogUrl** | **string** |  | 
**CollectionIds** | **List&lt;string&gt;** |  | [optional] 
**CatalogId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

