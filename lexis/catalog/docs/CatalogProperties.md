# LexisDataLake.Model.CatalogProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OwnerId** | **int?** |  | 
**Description** | **string** |  | [optional] 
**CatalogTimestamp** | **string** |  | 
**CatalogUrl** | **string** |  | 
**CatalogId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

