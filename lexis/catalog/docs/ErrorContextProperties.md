# LexisDataLake.Model.ErrorContextProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Stage** | **string** |  | 
**RequestTimeEpoch** | **string** |  | 
**RequestId** | **string** |  | 
**ApiId** | **string** |  | 
**ResourceId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

