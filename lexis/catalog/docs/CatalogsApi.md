# LexisDataLake.Api.CatalogsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/catalogs/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCatalog**](CatalogsApi.md#createcatalog) | **POST** /{catalogId} | Create a Catalog
[**DeleteCatalog**](CatalogsApi.md#deletecatalog) | **DELETE** /{catalogId} | Delete a Catalog
[**DescribeCatalogs**](CatalogsApi.md#describecatalogs) | **GET** /describe | Describe Catalogs
[**GetCatalog**](CatalogsApi.md#getcatalog) | **GET** /{catalogId} | Get a Catalog
[**ListCatalogs**](CatalogsApi.md#listcatalogs) | **GET** / | List Catalogs
[**UpdateCatalog**](CatalogsApi.md#updatecatalog) | **PATCH** /{catalogId} | Update a Catalog

<a name="createcatalog"></a>
# **CreateCatalog**
> CatalogResponse CreateCatalog (string catalogId, string description = null)

Create a Catalog

Creates a new catalog with a user specified catalog-id. The value catalog-id cannot be changed after a catalog is created.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateCatalogExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var catalogId = catalogId_example;  // string | The id of a catalog
            var description = description_example;  // string | The description of the resource (optional) 

            try
            {
                // Create a Catalog
                CatalogResponse result = apiInstance.CreateCatalog(catalogId, description);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.CreateCatalog: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalogId** | **string**| The id of a catalog | 
 **description** | **string**| The description of the resource | [optional] 

### Return type

[**CatalogResponse**](CatalogResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deletecatalog"></a>
# **DeleteCatalog**
> CatalogResponse DeleteCatalog (string catalogId)

Delete a Catalog

Deletes a catalog.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteCatalogExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var catalogId = catalogId_example;  // string | The id of a catalog

            try
            {
                // Delete a Catalog
                CatalogResponse result = apiInstance.DeleteCatalog(catalogId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.DeleteCatalog: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalogId** | **string**| The id of a catalog | 

### Return type

[**CatalogResponse**](CatalogResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="describecatalogs"></a>
# **DescribeCatalogs**
> DescribeCatalogsResponse DescribeCatalogs (string maxItems = null, string ownerId = null, string nextToken = null, string catalogId = null)

Describe Catalogs

Returns properties of a list of catalogs, including a list of collection-ids, and a truncated flag indicating if that list of collection-ids was truncated.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DescribeCatalogsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var ownerId = ownerId_example;  // string | The id of an owner (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 
            var catalogId = catalogId_example;  // string | The id of a catalog (optional) 

            try
            {
                // Describe Catalogs
                DescribeCatalogsResponse result = apiInstance.DescribeCatalogs(maxItems, ownerId, nextToken, catalogId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.DescribeCatalogs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **ownerId** | **string**| The id of an owner | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 
 **catalogId** | **string**| The id of a catalog | [optional] 

### Return type

[**DescribeCatalogsResponse**](DescribeCatalogsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getcatalog"></a>
# **GetCatalog**
> CatalogResponse GetCatalog (string catalogId)

Get a Catalog

Gets a catalog by ID.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetCatalogExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var catalogId = catalogId_example;  // string | The id of a catalog

            try
            {
                // Get a Catalog
                CatalogResponse result = apiInstance.GetCatalog(catalogId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.GetCatalog: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catalogId** | **string**| The id of a catalog | 

### Return type

[**CatalogResponse**](CatalogResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="listcatalogs"></a>
# **ListCatalogs**
> ListCatalogsResponse ListCatalogs (string maxItems = null, string ownerId = null, string nextToken = null)

List Catalogs

Returns a list of catalogs. You can filter down by owner-id.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ListCatalogsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var ownerId = ownerId_example;  // string | The id of an owner (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 

            try
            {
                // List Catalogs
                ListCatalogsResponse result = apiInstance.ListCatalogs(maxItems, ownerId, nextToken);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.ListCatalogs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **ownerId** | **string**| The id of an owner | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 

### Return type

[**ListCatalogsResponse**](ListCatalogsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updatecatalog"></a>
# **UpdateCatalog**
> CatalogResponse UpdateCatalog (UpdateCatalogRequest body, string catalogId)

Update a Catalog

Updates a catalog by ID. There are three patch operations: 'op'= 'replace' | 'add' | 'remove'. The operation 'replace' only applies to 'path'=/'description' to specify a new catalog description in 'value' = 'new description'.The operations 'add' and 'remove' can be used to insert and delete collections into the catalog, respectively. The list of collections to add/remove must be specified in 'value' for path='/collection-ids'. The combined total of collection ids in add and remove should not exceed 1000. For more information about JSON PATCH document please refer to RFC6902 https://tools.ietf.org/html/rfc6902.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateCatalogExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new CatalogsApi();
            var body = new UpdateCatalogRequest(); // UpdateCatalogRequest | Catalog to update
            var catalogId = catalogId_example;  // string | The id of a catalog

            try
            {
                // Update a Catalog
                CatalogResponse result = apiInstance.UpdateCatalog(body, catalogId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CatalogsApi.UpdateCatalog: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateCatalogRequest**](UpdateCatalogRequest.md)| Catalog to update | 
 **catalogId** | **string**| The id of a catalog | 

### Return type

[**CatalogResponse**](CatalogResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
