# LexisDataLake.Model.CatalogResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Catalog** | [**CatalogProperties**](CatalogProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

