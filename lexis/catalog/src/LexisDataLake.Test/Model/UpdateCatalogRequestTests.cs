/* 
 * DataLakeApi-release-Catalog
 *
 * A catalog is a group of one or more collections. The Catalog APIs provide functionality to create, get, list, update and remove a catalog.
 *
 * OpenAPI spec version: 2020-10-13T01:27:07Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using LexisDataLake.Api;
using LexisDataLake.Model;
using LexisDataLake.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing UpdateCatalogRequest
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class UpdateCatalogRequestTests
    {
        // TODO uncomment below to declare an instance variable for UpdateCatalogRequest
        //private UpdateCatalogRequest instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of UpdateCatalogRequest
            //instance = new UpdateCatalogRequest();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of UpdateCatalogRequest
        /// </summary>
        [Test]
        public void UpdateCatalogRequestInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" UpdateCatalogRequest
            //Assert.IsInstanceOfType<UpdateCatalogRequest> (instance, "variable 'instance' is a UpdateCatalogRequest");
        }


        /// <summary>
        /// Test the property 'PatchOperations'
        /// </summary>
        [Test]
        public void PatchOperationsTest()
        {
            // TODO unit test for the property 'PatchOperations'
        }

    }

}
