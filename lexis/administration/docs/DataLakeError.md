# LexisDataLake.Model.DataLakeError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Context** | [**ErrorContextProperties**](ErrorContextProperties.md) |  | 
**Error** | [**ErrorProperties**](ErrorProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

