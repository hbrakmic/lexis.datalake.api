# LexisDataLake.Model.OwnerResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Owner** | [**OwnerProperties**](OwnerProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

