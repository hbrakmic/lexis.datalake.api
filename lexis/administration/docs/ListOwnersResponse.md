# LexisDataLake.Model.ListOwnersResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextToken** | **string** |  | [optional] 
**ItemCount** | **int?** |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Owners** | [**List&lt;OwnerProperties&gt;**](OwnerProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

