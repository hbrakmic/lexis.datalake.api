# LexisDataLake.Model.UpdateOwnerRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PatchOperations** | [**List&lt;UpdateOwnerRequestPatchoperations&gt;**](UpdateOwnerRequestPatchoperations.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

