# LexisDataLake.Api.OwnersApi

All URIs are relative to *https://datalake.content.aws.lexis.com/administration/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateOwner**](OwnersApi.md#createowner) | **POST** /owners | Create a new Owner
[**DeleteOwner**](OwnersApi.md#deleteowner) | **DELETE** /owners/{ownerId} | Delete an Owner
[**GetOwner**](OwnersApi.md#getowner) | **GET** /owners/{ownerId} | Get an Owner by ID
[**ListOwners**](OwnersApi.md#listowners) | **GET** /owners | List Owners
[**UpdateOwner**](OwnersApi.md#updateowner) | **PATCH** /owners/{ownerId} | Update an existing Owner
[**Whoami**](OwnersApi.md#whoami) | **GET** /owners/whoami | Get owner details by API key

<a name="createowner"></a>
# **CreateOwner**
> CreateOwnerResponse CreateOwner (CreateOwnerRequest body)

Create a new Owner

Creates an owner to be used to interact with the DataLake.<br><b> Note: API key is returned only on creation of owner. For email-distribution provide email-ids or distribution-lists that are part of RELX.</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateOwnerExample
    {
        public void main()
        {
            var apiInstance = new OwnersApi();
            var body = new CreateOwnerRequest(); // CreateOwnerRequest | Owner to create

            try
            {
                // Create a new Owner
                CreateOwnerResponse result = apiInstance.CreateOwner(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.CreateOwner: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateOwnerRequest**](CreateOwnerRequest.md)| Owner to create | 

### Return type

[**CreateOwnerResponse**](CreateOwnerResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deleteowner"></a>
# **DeleteOwner**
> OwnerResponse DeleteOwner (string ownerId)

Delete an Owner

Delete your owner. All collections must be Terminated before calling Delete Owner

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteOwnerExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new OwnersApi();
            var ownerId = ownerId_example;  // string | The id of an owner

            try
            {
                // Delete an Owner
                OwnerResponse result = apiInstance.DeleteOwner(ownerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.DeleteOwner: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ownerId** | **string**| The id of an owner | 

### Return type

[**OwnerResponse**](OwnerResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getowner"></a>
# **GetOwner**
> OwnerResponse GetOwner (string ownerId)

Get an Owner by ID

Return details about an owner id. API Key information is not included. If you need your API Key recovered contact the DataLake team

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetOwnerExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new OwnersApi();
            var ownerId = ownerId_example;  // string | The id of an owner

            try
            {
                // Get an Owner by ID
                OwnerResponse result = apiInstance.GetOwner(ownerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.GetOwner: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ownerId** | **string**| The id of an owner | 

### Return type

[**OwnerResponse**](OwnerResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="listowners"></a>
# **ListOwners**
> ListOwnersResponse ListOwners (string maxItems = null, string nextToken = null)

List Owners

Returns a list of owners

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ListOwnersExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new OwnersApi();
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 

            try
            {
                // List Owners
                ListOwnersResponse result = apiInstance.ListOwners(maxItems, nextToken);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.ListOwners: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 

### Return type

[**ListOwnersResponse**](ListOwnersResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updateowner"></a>
# **UpdateOwner**
> OwnerResponse UpdateOwner (UpdateOwnerRequest body, string ownerId)

Update an existing Owner

Update an owner by ID. Data type of value is determined by the data type of the path.<br><b> Note: For email-distribution provide email-ids or distribution-lists that are part of RELX.</b>

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateOwnerExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new OwnersApi();
            var body = new UpdateOwnerRequest(); // UpdateOwnerRequest | Owner to update
            var ownerId = ownerId_example;  // string | The id of an owner

            try
            {
                // Update an existing Owner
                OwnerResponse result = apiInstance.UpdateOwner(body, ownerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.UpdateOwner: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateOwnerRequest**](UpdateOwnerRequest.md)| Owner to update | 
 **ownerId** | **string**| The id of an owner | 

### Return type

[**OwnerResponse**](OwnerResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="whoami"></a>
# **Whoami**
> OwnerResponse Whoami (string xApiKey)

Get owner details by API key

Returns back your owner details based on your current API key without having to know your owner-id

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class WhoamiExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new OwnersApi();
            var xApiKey = xApiKey_example;  // string | 

            try
            {
                // Get owner details by API key
                OwnerResponse result = apiInstance.Whoami(xApiKey);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OwnersApi.Whoami: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xApiKey** | **string**|  | 

### Return type

[**OwnerResponse**](OwnerResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
