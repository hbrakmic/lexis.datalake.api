# LexisDataLake.Model.CreateOwnerResponseOwner
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OwnerName** | **string** |  | 
**EmailDistribution** | **List&lt;string&gt;** |  | 
**ApiKey** | **string** |  | 
**CollectionPrefixes** | **List&lt;string&gt;** |  | [optional] 
**OwnerId** | **int?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

