# LexisDataLake.Model.CreateOwnerRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OwnerName** | **string** |  | 
**EmailDistribution** | **List&lt;string&gt;** |  | 
**CollectionPrefixes** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

