/* 
 * DataLakeApi-release-Administration
 *
 * The Administration API allows users to sign-up for an Owner or get supported Asset information. To get started with the DataLake you must have an Owner to retrieve or insert data into the lake. An api-key will be returned to use with any other API request
 *
 * OpenAPI spec version: 2020-10-08T00:06:42Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace LexisDataLake.Client
{
    /// <summary>
    /// <see cref="GlobalConfiguration"/> provides a compile-time extension point for globally configuring
    /// API Clients.
    /// </summary>
    /// <remarks>
    /// A customized implementation via partial class may reside in another file and may
    /// be excluded from automatic generation via a .swagger-codegen-ignore file.
    /// </remarks>
    public partial class GlobalConfiguration : Configuration
    {

    }
}