/* 
 * DataLakeApi-release-Administration
 *
 * The Administration API allows users to sign-up for an Owner or get supported Asset information. To get started with the DataLake you must have an Owner to retrieve or insert data into the lake. An api-key will be returned to use with any other API request
 *
 * OpenAPI spec version: 2020-10-08T00:06:42Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using LexisDataLake.Api;
using LexisDataLake.Model;
using LexisDataLake.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing OwnerResponse
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class OwnerResponseTests
    {
        // TODO uncomment below to declare an instance variable for OwnerResponse
        //private OwnerResponse instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of OwnerResponse
            //instance = new OwnerResponse();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of OwnerResponse
        /// </summary>
        [Test]
        public void OwnerResponseInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" OwnerResponse
            //Assert.IsInstanceOfType<OwnerResponse> (instance, "variable 'instance' is a OwnerResponse");
        }


        /// <summary>
        /// Test the property 'Owner'
        /// </summary>
        [Test]
        public void OwnerTest()
        {
            // TODO unit test for the property 'Owner'
        }
        /// <summary>
        /// Test the property 'Context'
        /// </summary>
        [Test]
        public void ContextTest()
        {
            // TODO unit test for the property 'Context'
        }

    }

}
