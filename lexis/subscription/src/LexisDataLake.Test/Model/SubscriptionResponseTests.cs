/* 
 * DataLakeApi-release-Subscription
 *
 * The Subscription API allows users to subscribe to events that impact Collections, Catalogs, or Objects
 *
 * OpenAPI spec version: 2020-12-10T16:25:59Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using LexisDataLake.Api;
using LexisDataLake.Model;
using LexisDataLake.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace LexisDataLake.Test
{
    /// <summary>
    ///  Class for testing SubscriptionResponse
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class SubscriptionResponseTests
    {
        // TODO uncomment below to declare an instance variable for SubscriptionResponse
        //private SubscriptionResponse instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of SubscriptionResponse
            //instance = new SubscriptionResponse();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of SubscriptionResponse
        /// </summary>
        [Test]
        public void SubscriptionResponseInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" SubscriptionResponse
            //Assert.IsInstanceOfType<SubscriptionResponse> (instance, "variable 'instance' is a SubscriptionResponse");
        }


        /// <summary>
        /// Test the property 'Context'
        /// </summary>
        [Test]
        public void ContextTest()
        {
            // TODO unit test for the property 'Context'
        }
        /// <summary>
        /// Test the property 'Subscription'
        /// </summary>
        [Test]
        public void SubscriptionTest()
        {
            // TODO unit test for the property 'Subscription'
        }

    }

}
