/* 
 * DataLakeApi-release-Subscription
 *
 * The Subscription API allows users to subscribe to events that impact Collections, Catalogs, or Objects
 *
 * OpenAPI spec version: 2020-12-10T16:25:59Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = LexisDataLake.Client.SwaggerDateConverter;

namespace LexisDataLake.Model
{
    /// <summary>
    /// RepublishProperties
    /// </summary>
    [DataContract]
        public partial class RepublishProperties :  IEquatable<RepublishProperties>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepublishProperties" /> class.
        /// </summary>
        /// <param name="catalogIds">catalogIds.</param>
        /// <param name="republishExpirationDate">republishExpirationDate (required).</param>
        /// <param name="subscriptionId">subscriptionId (required).</param>
        /// <param name="republishState">republishState (required).</param>
        /// <param name="republishTimestamp">republishTimestamp (required).</param>
        /// <param name="collectionIds">collectionIds.</param>
        /// <param name="republishId">republishId (required).</param>
        public RepublishProperties(List<string> catalogIds = default(List<string>), DateTime? republishExpirationDate = default(DateTime?), int? subscriptionId = default(int?), string republishState = default(string), DateTime? republishTimestamp = default(DateTime?), List<string> collectionIds = default(List<string>), string republishId = default(string))
        {
            // to ensure "republishExpirationDate" is required (not null)
            if (republishExpirationDate == null)
            {
                throw new InvalidDataException("republishExpirationDate is a required property for RepublishProperties and cannot be null");
            }
            else
            {
                this.RepublishExpirationDate = republishExpirationDate;
            }
            // to ensure "subscriptionId" is required (not null)
            if (subscriptionId == null)
            {
                throw new InvalidDataException("subscriptionId is a required property for RepublishProperties and cannot be null");
            }
            else
            {
                this.SubscriptionId = subscriptionId;
            }
            // to ensure "republishState" is required (not null)
            if (republishState == null)
            {
                throw new InvalidDataException("republishState is a required property for RepublishProperties and cannot be null");
            }
            else
            {
                this.RepublishState = republishState;
            }
            // to ensure "republishTimestamp" is required (not null)
            if (republishTimestamp == null)
            {
                throw new InvalidDataException("republishTimestamp is a required property for RepublishProperties and cannot be null");
            }
            else
            {
                this.RepublishTimestamp = republishTimestamp;
            }
            // to ensure "republishId" is required (not null)
            if (republishId == null)
            {
                throw new InvalidDataException("republishId is a required property for RepublishProperties and cannot be null");
            }
            else
            {
                this.RepublishId = republishId;
            }
            this.CatalogIds = catalogIds;
            this.CollectionIds = collectionIds;
        }
        
        /// <summary>
        /// Gets or Sets CatalogIds
        /// </summary>
        [DataMember(Name="catalog-ids", EmitDefaultValue=false)]
        public List<string> CatalogIds { get; set; }

        /// <summary>
        /// Gets or Sets RepublishExpirationDate
        /// </summary>
        [DataMember(Name="republish-expiration-date", EmitDefaultValue=false)]
        public DateTime? RepublishExpirationDate { get; set; }

        /// <summary>
        /// Gets or Sets SubscriptionId
        /// </summary>
        [DataMember(Name="subscription-id", EmitDefaultValue=false)]
        public int? SubscriptionId { get; set; }

        /// <summary>
        /// Gets or Sets RepublishState
        /// </summary>
        [DataMember(Name="republish-state", EmitDefaultValue=false)]
        public string RepublishState { get; set; }

        /// <summary>
        /// Gets or Sets RepublishTimestamp
        /// </summary>
        [DataMember(Name="republish-timestamp", EmitDefaultValue=false)]
        public DateTime? RepublishTimestamp { get; set; }

        /// <summary>
        /// Gets or Sets CollectionIds
        /// </summary>
        [DataMember(Name="collection-ids", EmitDefaultValue=false)]
        public List<string> CollectionIds { get; set; }

        /// <summary>
        /// Gets or Sets RepublishId
        /// </summary>
        [DataMember(Name="republish-id", EmitDefaultValue=false)]
        public string RepublishId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class RepublishProperties {\n");
            sb.Append("  CatalogIds: ").Append(CatalogIds).Append("\n");
            sb.Append("  RepublishExpirationDate: ").Append(RepublishExpirationDate).Append("\n");
            sb.Append("  SubscriptionId: ").Append(SubscriptionId).Append("\n");
            sb.Append("  RepublishState: ").Append(RepublishState).Append("\n");
            sb.Append("  RepublishTimestamp: ").Append(RepublishTimestamp).Append("\n");
            sb.Append("  CollectionIds: ").Append(CollectionIds).Append("\n");
            sb.Append("  RepublishId: ").Append(RepublishId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as RepublishProperties);
        }

        /// <summary>
        /// Returns true if RepublishProperties instances are equal
        /// </summary>
        /// <param name="input">Instance of RepublishProperties to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(RepublishProperties input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.CatalogIds == input.CatalogIds ||
                    this.CatalogIds != null &&
                    input.CatalogIds != null &&
                    this.CatalogIds.SequenceEqual(input.CatalogIds)
                ) && 
                (
                    this.RepublishExpirationDate == input.RepublishExpirationDate ||
                    (this.RepublishExpirationDate != null &&
                    this.RepublishExpirationDate.Equals(input.RepublishExpirationDate))
                ) && 
                (
                    this.SubscriptionId == input.SubscriptionId ||
                    (this.SubscriptionId != null &&
                    this.SubscriptionId.Equals(input.SubscriptionId))
                ) && 
                (
                    this.RepublishState == input.RepublishState ||
                    (this.RepublishState != null &&
                    this.RepublishState.Equals(input.RepublishState))
                ) && 
                (
                    this.RepublishTimestamp == input.RepublishTimestamp ||
                    (this.RepublishTimestamp != null &&
                    this.RepublishTimestamp.Equals(input.RepublishTimestamp))
                ) && 
                (
                    this.CollectionIds == input.CollectionIds ||
                    this.CollectionIds != null &&
                    input.CollectionIds != null &&
                    this.CollectionIds.SequenceEqual(input.CollectionIds)
                ) && 
                (
                    this.RepublishId == input.RepublishId ||
                    (this.RepublishId != null &&
                    this.RepublishId.Equals(input.RepublishId))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.CatalogIds != null)
                    hashCode = hashCode * 59 + this.CatalogIds.GetHashCode();
                if (this.RepublishExpirationDate != null)
                    hashCode = hashCode * 59 + this.RepublishExpirationDate.GetHashCode();
                if (this.SubscriptionId != null)
                    hashCode = hashCode * 59 + this.SubscriptionId.GetHashCode();
                if (this.RepublishState != null)
                    hashCode = hashCode * 59 + this.RepublishState.GetHashCode();
                if (this.RepublishTimestamp != null)
                    hashCode = hashCode * 59 + this.RepublishTimestamp.GetHashCode();
                if (this.CollectionIds != null)
                    hashCode = hashCode * 59 + this.CollectionIds.GetHashCode();
                if (this.RepublishId != null)
                    hashCode = hashCode * 59 + this.RepublishId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
