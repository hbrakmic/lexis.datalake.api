/* 
 * DataLakeApi-release-Subscription
 *
 * The Subscription API allows users to subscribe to events that impact Collections, Catalogs, or Objects
 *
 * OpenAPI spec version: 2020-12-10T16:25:59Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = LexisDataLake.Client.SwaggerDateConverter;

namespace LexisDataLake.Model
{
    /// <summary>
    /// CreateSubscriptionRequestFilter
    /// </summary>
    [DataContract]
        public partial class CreateSubscriptionRequestFilter :  IEquatable<CreateSubscriptionRequestFilter>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateSubscriptionRequestFilter" /> class.
        /// </summary>
        /// <param name="collectionId">collectionId.</param>
        /// <param name="eventName">eventName.</param>
        /// <param name="catalogId">catalogId.</param>
        public CreateSubscriptionRequestFilter(List<Object> collectionId = default(List<Object>), List<string> eventName = default(List<string>), List<Object> catalogId = default(List<Object>))
        {
            this.CollectionId = collectionId;
            this.EventName = eventName;
            this.CatalogId = catalogId;
        }
        
        /// <summary>
        /// Gets or Sets CollectionId
        /// </summary>
        [DataMember(Name="collection-id", EmitDefaultValue=false)]
        public List<Object> CollectionId { get; set; }

        /// <summary>
        /// Gets or Sets EventName
        /// </summary>
        [DataMember(Name="event-name", EmitDefaultValue=false)]
        public List<string> EventName { get; set; }

        /// <summary>
        /// Gets or Sets CatalogId
        /// </summary>
        [DataMember(Name="catalog-id", EmitDefaultValue=false)]
        public List<Object> CatalogId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CreateSubscriptionRequestFilter {\n");
            sb.Append("  CollectionId: ").Append(CollectionId).Append("\n");
            sb.Append("  EventName: ").Append(EventName).Append("\n");
            sb.Append("  CatalogId: ").Append(CatalogId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as CreateSubscriptionRequestFilter);
        }

        /// <summary>
        /// Returns true if CreateSubscriptionRequestFilter instances are equal
        /// </summary>
        /// <param name="input">Instance of CreateSubscriptionRequestFilter to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CreateSubscriptionRequestFilter input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.CollectionId == input.CollectionId ||
                    this.CollectionId != null &&
                    input.CollectionId != null &&
                    this.CollectionId.SequenceEqual(input.CollectionId)
                ) && 
                (
                    this.EventName == input.EventName ||
                    this.EventName != null &&
                    input.EventName != null &&
                    this.EventName.SequenceEqual(input.EventName)
                ) && 
                (
                    this.CatalogId == input.CatalogId ||
                    this.CatalogId != null &&
                    input.CatalogId != null &&
                    this.CatalogId.SequenceEqual(input.CatalogId)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.CollectionId != null)
                    hashCode = hashCode * 59 + this.CollectionId.GetHashCode();
                if (this.EventName != null)
                    hashCode = hashCode * 59 + this.EventName.GetHashCode();
                if (this.CatalogId != null)
                    hashCode = hashCode * 59 + this.CatalogId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
