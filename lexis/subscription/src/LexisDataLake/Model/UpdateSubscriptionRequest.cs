/* 
 * DataLakeApi-release-Subscription
 *
 * The Subscription API allows users to subscribe to events that impact Collections, Catalogs, or Objects
 *
 * OpenAPI spec version: 2020-12-10T16:25:59Z
 * Contact: Wormhole-PlanetX@ReedElsevier.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = LexisDataLake.Client.SwaggerDateConverter;

namespace LexisDataLake.Model
{
    /// <summary>
    /// UpdateSubscriptionRequest
    /// </summary>
    [DataContract]
        public partial class UpdateSubscriptionRequest :  IEquatable<UpdateSubscriptionRequest>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateSubscriptionRequest" /> class.
        /// </summary>
        /// <param name="patchOperations">patchOperations (required).</param>
        public UpdateSubscriptionRequest(List<UpdateSubscriptionRequestPatchoperations> patchOperations = default(List<UpdateSubscriptionRequestPatchoperations>))
        {
            // to ensure "patchOperations" is required (not null)
            if (patchOperations == null)
            {
                throw new InvalidDataException("patchOperations is a required property for UpdateSubscriptionRequest and cannot be null");
            }
            else
            {
                this.PatchOperations = patchOperations;
            }
        }
        
        /// <summary>
        /// Gets or Sets PatchOperations
        /// </summary>
        [DataMember(Name="patch-operations", EmitDefaultValue=false)]
        public List<UpdateSubscriptionRequestPatchoperations> PatchOperations { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class UpdateSubscriptionRequest {\n");
            sb.Append("  PatchOperations: ").Append(PatchOperations).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as UpdateSubscriptionRequest);
        }

        /// <summary>
        /// Returns true if UpdateSubscriptionRequest instances are equal
        /// </summary>
        /// <param name="input">Instance of UpdateSubscriptionRequest to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(UpdateSubscriptionRequest input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.PatchOperations == input.PatchOperations ||
                    this.PatchOperations != null &&
                    input.PatchOperations != null &&
                    this.PatchOperations.SequenceEqual(input.PatchOperations)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.PatchOperations != null)
                    hashCode = hashCode * 59 + this.PatchOperations.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
