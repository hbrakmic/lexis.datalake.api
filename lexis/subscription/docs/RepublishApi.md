# LexisDataLake.Api.RepublishApi

All URIs are relative to *https://datalake.content.aws.lexis.com/subscriptions/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateRepublish**](RepublishApi.md#createrepublish) | **POST** /{subscriptionId}/republish | Create a Republish
[**GetRepublish**](RepublishApi.md#getrepublish) | **GET** /{subscriptionId}/republish/{republishId} | Get a Republish

<a name="createrepublish"></a>
# **CreateRepublish**
> RepublishResponse CreateRepublish (CreateRepublishRequest body, string subscriptionId)

Create a Republish

Starts a job to publish all objects within the collections or catalogs submitted. Can only include one of the aforementioned attributes.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateRepublishExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new RepublishApi();
            var body = new CreateRepublishRequest(); // CreateRepublishRequest | Republish operation to start
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Create a Republish
                RepublishResponse result = apiInstance.CreateRepublish(body, subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RepublishApi.CreateRepublish: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateRepublishRequest**](CreateRepublishRequest.md)| Republish operation to start | 
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**RepublishResponse**](RepublishResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getrepublish"></a>
# **GetRepublish**
> RepublishResponse GetRepublish (string republishId, string subscriptionId)

Get a Republish

Gets a republish request.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetRepublishExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new RepublishApi();
            var republishId = republishId_example;  // string | The id of a republish request
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Get a Republish
                RepublishResponse result = apiInstance.GetRepublish(republishId, subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RepublishApi.GetRepublish: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **republishId** | **string**| The id of a republish request | 
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**RepublishResponse**](RepublishResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
