# LexisDataLake.Model.UpdateSubscriptionRequestPatchoperations
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Op** | **string** | The operation to be performed | 
**Path** | **string** | The path of the json object to replace | 
**Value** | **Object** | The value used by the operation. Expected type depends on path: &lt;br&gt; /subscription-name&#x3D;string &lt;br&gt; /filter&#x3D;Object with optional keys: \&quot;event-name\&quot;, \&quot;collection-id\&quot; and \&quot;catalog-id\&quot;. Optional keys have a type of array(string). &lt;br&gt; /schema-version&#x3D;array(string) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

