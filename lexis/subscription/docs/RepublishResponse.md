# LexisDataLake.Model.RepublishResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Republish** | [**RepublishProperties**](RepublishProperties.md) |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

