# LexisDataLake.Model.CreateSubscriptionRequestFilter
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CollectionId** | **List&lt;Object&gt;** |  | [optional] 
**EventName** | **List&lt;string&gt;** |  | [optional] 
**CatalogId** | **List&lt;Object&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

