# LexisDataLake.Model.CreateSubscriptionRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Filter** | [**CreateSubscriptionRequestFilter**](CreateSubscriptionRequestFilter.md) |  | [optional] 
**Endpoint** | **string** |  | 
**Protocol** | **string** |  | 
**SubscriptionName** | **string** |  | 
**SchemaVersion** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

