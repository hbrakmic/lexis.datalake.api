# LexisDataLake.Api.SubscriptionsApi

All URIs are relative to *https://datalake.content.aws.lexis.com/subscriptions/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateSubscription**](SubscriptionsApi.md#createsubscription) | **POST** / | Create a Subscription
[**DeleteSubscription**](SubscriptionsApi.md#deletesubscription) | **DELETE** /{subscriptionId} | Delete a Subscription
[**GetSubscription**](SubscriptionsApi.md#getsubscription) | **GET** /{subscriptionId} | Get a Subscription by ID
[**ListSubscriptions**](SubscriptionsApi.md#listsubscriptions) | **GET** / | List Subscriptions
[**ResendSubscription**](SubscriptionsApi.md#resendsubscription) | **POST** /{subscriptionId}/resend | Resend a Subscription Confirmation
[**UpdateSubscription**](SubscriptionsApi.md#updatesubscription) | **PATCH** /{subscriptionId} | Update a Subscription

<a name="createsubscription"></a>
# **CreateSubscription**
> CreateSubscriptionResponse CreateSubscription (CreateSubscriptionRequest body)

Create a Subscription

Creates a new subscription for the given sqs or email. **BE SURE PERMISSIONS ARE SET PROPERLY FIRST!!** Read more here: https://datalake.content.aws.lexis.com/subscription/dev-guide/creating-subscription.html. <br> Note: If you add filtering on collection-id/catalog-id and Subscription::* event-name you will not receive any Subscription::* events.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class CreateSubscriptionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var body = new CreateSubscriptionRequest(); // CreateSubscriptionRequest | Subscription to create

            try
            {
                // Create a Subscription
                CreateSubscriptionResponse result = apiInstance.CreateSubscription(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.CreateSubscription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateSubscriptionRequest**](CreateSubscriptionRequest.md)| Subscription to create | 

### Return type

[**CreateSubscriptionResponse**](CreateSubscriptionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="deletesubscription"></a>
# **DeleteSubscription**
> SubscriptionResponse DeleteSubscription (string subscriptionId)

Delete a Subscription

Deletes a subscription

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class DeleteSubscriptionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Delete a Subscription
                SubscriptionResponse result = apiInstance.DeleteSubscription(subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.DeleteSubscription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**SubscriptionResponse**](SubscriptionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="getsubscription"></a>
# **GetSubscription**
> SubscriptionResponse GetSubscription (string subscriptionId)

Get a Subscription by ID

Gets a subscription

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class GetSubscriptionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Get a Subscription by ID
                SubscriptionResponse result = apiInstance.GetSubscription(subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.GetSubscription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**SubscriptionResponse**](SubscriptionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="listsubscriptions"></a>
# **ListSubscriptions**
> ListSubscriptionsResponse ListSubscriptions (string maxItems = null, string ownerId = null, string nextToken = null)

List Subscriptions

Returns a list of subscriptions. You can filter down to an owner.

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ListSubscriptionsExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var maxItems = maxItems_example;  // string | Max number of records to return per call (optional) 
            var ownerId = ownerId_example;  // string | The id of an owner (optional) 
            var nextToken = nextToken_example;  // string | next-token from a previous request to continue paginating through the results (optional) 

            try
            {
                // List Subscriptions
                ListSubscriptionsResponse result = apiInstance.ListSubscriptions(maxItems, ownerId, nextToken);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.ListSubscriptions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **maxItems** | **string**| Max number of records to return per call | [optional] 
 **ownerId** | **string**| The id of an owner | [optional] 
 **nextToken** | **string**| next-token from a previous request to continue paginating through the results | [optional] 

### Return type

[**ListSubscriptionsResponse**](ListSubscriptionsResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="resendsubscription"></a>
# **ResendSubscription**
> SubscriptionResponse ResendSubscription (string subscriptionId)

Resend a Subscription Confirmation

Resend a subscription confirmation message

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class ResendSubscriptionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Resend a Subscription Confirmation
                SubscriptionResponse result = apiInstance.ResendSubscription(subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.ResendSubscription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**SubscriptionResponse**](SubscriptionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="updatesubscription"></a>
# **UpdateSubscription**
> SubscriptionResponse UpdateSubscription (UpdateSubscriptionRequest body, string subscriptionId)

Update a Subscription

Updates a subscription

### Example
```csharp
using System;
using System.Diagnostics;
using LexisDataLake.Api;
using LexisDataLake.Client;
using LexisDataLake.Model;

namespace Example
{
    public class UpdateSubscriptionExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new SubscriptionsApi();
            var body = new UpdateSubscriptionRequest(); // UpdateSubscriptionRequest | Subscription to update
            var subscriptionId = subscriptionId_example;  // string | The id of a subscription

            try
            {
                // Update a Subscription
                SubscriptionResponse result = apiInstance.UpdateSubscription(body, subscriptionId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SubscriptionsApi.UpdateSubscription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateSubscriptionRequest**](UpdateSubscriptionRequest.md)| Subscription to update | 
 **subscriptionId** | **string**| The id of a subscription | 

### Return type

[**SubscriptionResponse**](SubscriptionResponse.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
