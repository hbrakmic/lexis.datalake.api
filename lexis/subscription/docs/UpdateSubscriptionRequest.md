# LexisDataLake.Model.UpdateSubscriptionRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PatchOperations** | [**List&lt;UpdateSubscriptionRequestPatchoperations&gt;**](UpdateSubscriptionRequestPatchoperations.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

