# LexisDataLake.Model.RepublishProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CatalogIds** | **List&lt;string&gt;** |  | [optional] 
**RepublishExpirationDate** | **DateTime?** |  | 
**SubscriptionId** | **int?** |  | 
**RepublishState** | **string** |  | 
**RepublishTimestamp** | **DateTime?** |  | 
**CollectionIds** | **List&lt;string&gt;** |  | [optional] 
**RepublishId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

