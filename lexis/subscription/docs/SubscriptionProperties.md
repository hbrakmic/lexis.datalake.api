# LexisDataLake.Model.SubscriptionProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Filter** | [**CreateSubscriptionRequestFilter**](CreateSubscriptionRequestFilter.md) |  | [optional] 
**Protocol** | **string** |  | 
**Endpoint** | **string** |  | 
**SubscriptionId** | **int?** |  | 
**SubscriptionName** | **string** |  | 
**SubscriptionState** | **string** |  | 
**SchemaVersion** | **List&lt;string&gt;** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

