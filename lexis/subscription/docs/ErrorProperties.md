# LexisDataLake.Model.ErrorProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StackTrace** | **List&lt;string&gt;** |  | [optional] 
**CorrectiveAction** | **string** |  | [optional] 
**Message** | **string** |  | 
**Type** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

