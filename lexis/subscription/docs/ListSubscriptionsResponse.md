# LexisDataLake.Model.ListSubscriptionsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextToken** | **string** |  | [optional] 
**Subscriptions** | [**List&lt;SubscriptionProperties&gt;**](SubscriptionProperties.md) |  | 
**ItemCount** | **int?** |  | 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

