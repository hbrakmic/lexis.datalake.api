# LexisDataLake.Model.CompletionMetadataProperties
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NextAction** | **string** |  | 
**Protocol** | **string** |  | [optional] 
**QueueUrl** | **string** |  | [optional] 
**SubscriptionExpirationDate** | **string** |  | [optional] 
**SqsPolicy** | **Object** |  | [optional] 
**TopicArn** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

