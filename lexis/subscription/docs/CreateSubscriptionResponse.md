# LexisDataLake.Model.CreateSubscriptionResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CompletionMetadata** | [**CompletionMetadataProperties**](CompletionMetadataProperties.md) |  | [optional] 
**Context** | [**ContextProperties**](ContextProperties.md) |  | 
**Subscription** | [**SubscriptionProperties**](SubscriptionProperties.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

