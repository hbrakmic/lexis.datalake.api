﻿using LexisDataLake.Api;
using LexisDataLake.Model;
using System;

namespace Administration
{
    class Program
    {
        static void Main(string[] args)
        {
            var api = new OwnersApi();

            var request = new CreateOwnerRequest
            (
                "YOUR_NAME",
                new System.Collections.Generic.List<string> { "your_email@patentsight.com" },
                new System.Collections.Generic.List<string> { "YOUR_PREFIXES" }
            );

            var response = api.CreateOwner(request);
            Console.WriteLine(response.ToJson());

            Console.ReadLine();
        }
    }
}
