﻿using System;
using LexisDataLake.Api;
using LexisDataLake.Model;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            var api = new CollectionsApi();
            api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

            var request = new CreateCollectionRequest(128, false, 5, "my brand new collection", "Content", 10, 2);

            var result = api.CreateCollection(request, "collection123");

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
