﻿using System;
using LexisDataLake.Api;
using LexisDataLake.Model;

namespace Catalogs
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateCatalog();
            ListCatalogs();

            Console.ReadLine();
        }

        static void CreateCatalog()
        {
            var api = new CatalogsApi();
            api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

            var result = api.CreateCatalog("catalog12345", "my brand new catalog");

            Console.WriteLine(result.ToJson());
        }

        static void ListCatalogs()
        {
            var api = new CatalogsApi();
            api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

            var result = api.ListCatalogs();

            Console.WriteLine(result.ToJson());
        }
    }
}
