# Lexis DataLake API for .NET


This solution contains libraries for accessing [Lexis DataLake APIs](https://datalake-staging.content.aws.lexis.com/). 

There are also a few demo projects available.

![lexis_datalake_solution](lexis_datalake_solution.png)

### Usage

**Creating Owners**

You will have to create [a new Owner](https://datalake-staging.content.aws.lexis.com/admin/dev-guide/creating-owner.html) to be able to use this API. The demo project "Administration" shows how to complete this task.

```csharp
static void Main(string[] args)
{
    var api = new OwnersApi();

    var request = new CreateOwnerRequest
    (
        "YOUR_NAME",
        new System.Collections.Generic.List<string> { "your_email@patentsight.com" },
        new System.Collections.Generic.List<string> { "YOUR_PREFIXES" }
    );

    var response = api.CreateOwner(request);
    Console.WriteLine(response.ToJson());

    Console.ReadLine();
}
```

**Creating Catalogs**

![create_catalog](create_catalog.png)

Use the CatalogApi to create [a new Catalog](https://datalake-staging.content.aws.lexis.com/catalog/api-reference/create-catalog.html) for your owner. The API-Key you received for your owner will be needed for successful execution.

```csharp
static void CreateCatalog()
{
    var api = new CatalogsApi();
    api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

    var result = api.CreateCatalog("catalog12345", "my brand new catalog");

    Console.WriteLine(result.ToJson());
}
```

**Listing Catalogs**

![list_catalogs](list_catalogs.png)

You can [list catalogs](https://datalake-staging.content.aws.lexis.com/catalog/api-reference/list-catalogs.html) by using this API call.

It is also possible to filter catalogs by Owner ID.

```csharp
static void ListCatalogs()
{
    var api = new CatalogsApi();
    api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

    var result = api.ListCatalogs();

    Console.WriteLine(result.ToJson());
}
```

**Creating Collections**

![create_collection](create_collection.png)

You can [create collections](https://datalake-staging.content.aws.lexis.com/collection/index.html) by using this API call.

```csharp
var api = new CollectionsApi();
api.Configuration.AddApiKey("x-api-key", "YOUR_API_KEY");

var request = new CreateCollectionRequest(128, false, 5, "my brand new collection", "Content", 10, 2);

var result = api.CreateCollection(request, "collection123");

Console.WriteLine(result);
```